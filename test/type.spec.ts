/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {assert, Has, IsAny, IsNever, NotHas} from 'conditional-type-checks';
import {SCThing, SCThingWithoutReferences} from '../src/things/abstract/thing';
import {SCAcademicEvent, SCAcademicEventWithoutReferences} from '../src/things/academic-event';
import {SCArticle, SCArticleWithoutReferences} from '../src/things/article';
import {SCAssessment, SCAssessmentWithoutReferences} from '../src/things/assessment';
import {SCBook, SCBookWithoutReferences} from '../src/things/book';
import {SCBuilding, SCBuildingWithoutReferences} from '../src/things/building';
import {SCCatalog, SCCatalogWithoutReferences} from '../src/things/catalog';
import {SCContactPoint, SCContactPointWithoutReferences} from '../src/things/contact-point';
import {SCCourseOfStudy, SCCourseOfStudyWithoutReferences} from '../src/things/course-of-study';
import {SCDateSeries, SCDateSeriesWithoutReferences} from '../src/things/date-series';
import {SCDiff, SCDiffWithoutReferences} from '../src/things/diff';
import {SCDish, SCDishWithoutReferences} from '../src/things/dish';
import {SCFavorite, SCFavoriteWithoutReferences} from '../src/things/favorite';
import {SCFloor, SCFloorWithoutReferences} from '../src/things/floor';
import {SCMessage, SCMessageWithoutReferences} from '../src/things/message';
import {SCOrganization, SCOrganizationWithoutReferences} from '../src/things/organization';
import {SCPeriodical, SCPeriodicalWithoutReferences} from '../src/things/periodical';
import {SCPerson, SCPersonWithoutReferences} from '../src/things/person';
import {SCPointOfInterest, SCPointOfInterestWithoutReferences} from '../src/things/point-of-interest';
import {SCPublicationEvent, SCPublicationEventWithoutReferences} from '../src/things/publication-event';
import {SCRoom, SCRoomWithoutReferences} from '../src/things/room';
import {SCSemester, SCSemesterWithoutReferences} from '../src/things/semester';
import {SCSetting, SCSettingWithoutReferences} from '../src/things/setting';
import {SCSportCourse, SCSportCourseWithoutReferences} from '../src/things/sport-course';
import {SCStudyModule, SCStudyModuleWithoutReferences} from '../src/things/study-module';
import {SCTicket, SCTicketWithoutReferences} from '../src/things/ticket';
import {SCToDo, SCToDoWithoutReferences} from '../src/things/todo';
import {SCTour, SCTourWithoutReferences} from '../src/things/tour';
import {SCVideo, SCVideoWithoutReferences} from '../src/things/video';

/**
 * Check if E extends T
 */
type Extends<E, T> = E extends T ? true : false;

/**
 * Get type of array elements up to nesting level 3
 */
type ElementType<T> = T extends any[]
  ? T[0] extends any[]
    ? T[0][0] extends any[]
      ? T[0][0][0]
      : T[0][0]
    : T[0]
  : T;

/**
 * Get types of properties
 *
 * - Extracts only the properties which extend object and are not any.
 * - If type is an array it returns the type of the elements.
 */
type PropertyTypes<T> = Extract<
  ElementType<T extends object ? (IsAny<T[keyof T]> extends true ? never : T[keyof T]) : never>,
  object
>;

/**
 * Get nested property types
 */
type PropertyTypesNested<T> = PropertyTypes<T> extends object
  ? PropertyTypes<PropertyTypes<T>>
  : PropertyTypes<T>;

/**
 * Types of properties of SCDiff
 */
type SCDiffPropertyTypes = PropertyTypesNested<SCDiff>;
assert<NotHas<SCDiffPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCDiffPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCDiffPropertyTypes, SCThing>>(true);
assert<Has<SCDiffPropertyTypes, SCThing>>(false);
assert<Extends<SCDiffWithoutReferences, SCThing>>(false);
assert<Extends<SCDiff, SCThing>>(true);

/**
 * Types of properties of SCAcademicEvent
 */
type SCAcademicEventPropertyTypes = PropertyTypesNested<SCAcademicEvent>;
assert<NotHas<SCAcademicEventPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCAcademicEventPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCAcademicEventPropertyTypes, SCThing>>(true);
assert<Has<SCAcademicEventPropertyTypes, SCThing>>(false);
assert<Extends<SCAcademicEventWithoutReferences, SCThing>>(false);
assert<Extends<SCAcademicEvent, SCThing>>(true);

/**
 * Types of properties of SCArticle
 */
type SCArticlePropertyTypes = PropertyTypesNested<SCArticle>;
assert<NotHas<SCArticlePropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCArticlePropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCArticlePropertyTypes, SCThing>>(true);
assert<Has<SCArticlePropertyTypes, SCThing>>(false);
assert<Extends<SCArticleWithoutReferences, SCThing>>(false);
assert<Extends<SCArticle, SCThing>>(true);

/**
 * Types of properties of SCAssessment
 */
type SCAssessmentPropertyTypes = PropertyTypesNested<SCAssessment>;
assert<NotHas<SCAssessmentPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCAssessmentPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCAssessmentPropertyTypes, SCThing>>(true);
assert<Has<SCAssessmentPropertyTypes, SCThing>>(false);
assert<Extends<SCAssessmentWithoutReferences, SCThing>>(false);
assert<Extends<SCAssessment, SCThing>>(true);

/**
 * Types of properties of SCBook
 */
type SCBookPropertyTypes = PropertyTypesNested<SCBook>;
assert<NotHas<SCBookPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCBookPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCBookPropertyTypes, SCThing>>(true);
assert<Has<SCBookPropertyTypes, SCThing>>(false);
assert<Extends<SCBookWithoutReferences, SCThing>>(false);
assert<Extends<SCBook, SCThing>>(true);

/**
 * Types of properties of SCBuilding
 */
type SCBuildingPropertyTypes = PropertyTypesNested<SCBuilding>;
assert<NotHas<SCBuildingPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCBuildingPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCBuildingPropertyTypes, SCThing>>(true);
assert<Has<SCBuildingPropertyTypes, SCThing>>(false);
assert<Extends<SCBuildingWithoutReferences, SCThing>>(false);
assert<Extends<SCBuilding, SCThing>>(true);

/**
 * Types of properties of SCContactPoint
 */
type SCContactPointPropertyTypes = PropertyTypesNested<SCContactPoint>;
assert<NotHas<SCContactPointPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCContactPointPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCContactPointPropertyTypes, SCThing>>(true);
assert<Has<SCContactPointPropertyTypes, SCThing>>(false);
assert<Extends<SCContactPointWithoutReferences, SCThing>>(false);
assert<Extends<SCContactPoint, SCThing>>(true);

/**
 * Types of properties of SCCatalog
 */
type SCCatalogPropertyTypes = PropertyTypesNested<SCCatalog>;
assert<NotHas<SCCatalogPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCCatalogPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCCatalogPropertyTypes, SCThing>>(true);
assert<Has<SCCatalogPropertyTypes, SCThing>>(false);
assert<Extends<SCCatalogWithoutReferences, SCThing>>(false);
assert<Extends<SCCatalog, SCThing>>(true);

/**
 * Types of properties of SCCourseOfStudy
 */
type SCCourseOfStudyPropertyTypes = PropertyTypesNested<SCCourseOfStudy>;
assert<NotHas<SCCourseOfStudyPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCCourseOfStudyPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCCourseOfStudyPropertyTypes, SCThing>>(true);
assert<Has<SCCourseOfStudyPropertyTypes, SCThing>>(false);
assert<Extends<SCCourseOfStudyWithoutReferences, SCThing>>(false);
assert<Extends<SCCourseOfStudy, SCThing>>(true);

/**
 * Types of properties of SCDateSeries
 */
type SCDateSeriesPropertyTypes = PropertyTypesNested<SCDateSeries>;
assert<NotHas<SCDateSeriesPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCDateSeriesPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCDateSeriesPropertyTypes, SCThing>>(true);
assert<Has<SCDateSeriesPropertyTypes, SCThing>>(false);
assert<Extends<SCDateSeriesWithoutReferences, SCThing>>(false);
assert<Extends<SCDateSeries, SCThing>>(true);

/**
 * Types of properties of SCDish
 */
type SCDishPropertyTypes = PropertyTypesNested<SCDish>;
assert<NotHas<SCDishPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCDishPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCDishPropertyTypes, SCThing>>(true);
assert<Has<SCDishPropertyTypes, SCThing>>(false);
assert<Extends<SCDishWithoutReferences, SCThing>>(false);
assert<Extends<SCDish, SCThing>>(true);

/**
 * Types of properties of SCFavorite
 */
type SCFavoritePropertyTypes = PropertyTypesNested<SCFavorite>;
assert<NotHas<SCFavoritePropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCFavoritePropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCFavoritePropertyTypes, SCThing>>(true);
assert<Has<SCFavoritePropertyTypes, SCThing>>(false);
assert<Extends<SCFavoriteWithoutReferences, SCThing>>(false);
assert<Extends<SCFavorite, SCThing>>(true);

/**
 * Types of properties of SCFloor
 */
type SCFloorPropertyTypes = PropertyTypesNested<SCFloor>;
assert<NotHas<SCFloorPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCFloorPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCFloorPropertyTypes, SCThing>>(true);
assert<Has<SCFloorPropertyTypes, SCThing>>(false);
assert<Extends<SCFloorWithoutReferences, SCThing>>(false);
assert<Extends<SCFloor, SCThing>>(true);

/**
 * Types of properties of SCMessage
 */
type SCMessagePropertyTypes = PropertyTypesNested<SCMessage>;
assert<NotHas<SCMessagePropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCMessagePropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCMessagePropertyTypes, SCThing>>(true);
assert<Has<SCMessagePropertyTypes, SCThing>>(false);
assert<Extends<SCMessageWithoutReferences, SCThing>>(false);
assert<Extends<SCMessage, SCThing>>(true);

/**
 * Types of properties of SCOrganization
 */
type SCOrganizationPropertyTypes = PropertyTypesNested<SCOrganization>;
assert<NotHas<SCOrganizationPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCOrganizationPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCOrganizationPropertyTypes, SCThing>>(true);
assert<Has<SCOrganizationPropertyTypes, SCThing>>(false);
assert<Extends<SCOrganizationWithoutReferences, SCThing>>(false);
assert<Extends<SCOrganization, SCThing>>(true);

/**
 * Types of properties of SCPeriodical
 */
type SCPeriodicalPropertyTypes = PropertyTypesNested<SCPeriodical>;
assert<NotHas<SCPeriodicalPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCPeriodicalPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCPeriodicalPropertyTypes, SCThing>>(true);
assert<Has<SCPeriodicalPropertyTypes, SCThing>>(false);
assert<Extends<SCPeriodicalWithoutReferences, SCThing>>(false);
assert<Extends<SCPeriodical, SCThing>>(true);

/**
 * Types of properties of SCPerson
 */
type SCPersonPropertyTypes = PropertyTypesNested<SCPerson>;
assert<NotHas<SCPersonPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCPersonPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCPersonPropertyTypes, SCThing>>(true);
assert<Has<SCPersonPropertyTypes, SCThing>>(false);
assert<Extends<SCPersonWithoutReferences, SCThing>>(false);
assert<Extends<SCPerson, SCThing>>(true);

/**
 * Types of properties of SCPointOfInterest
 */
type SCPointOfInterestPropertyTypes = PropertyTypesNested<SCPointOfInterest>;
assert<NotHas<SCPointOfInterestPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCPointOfInterestPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCPointOfInterestPropertyTypes, SCThing>>(true);
assert<Has<SCPointOfInterestPropertyTypes, SCThing>>(false);
assert<Extends<SCPointOfInterestWithoutReferences, SCThing>>(false);
assert<Extends<SCPointOfInterest, SCThing>>(true);

/**
 * Types of properties of SCPublicationEvent
 */
type SCPublicationEventPropertyTypes = PropertyTypesNested<SCPublicationEvent>;
assert<NotHas<SCPublicationEventPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCPublicationEventPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCPublicationEventPropertyTypes, SCThing>>(true);
assert<Has<SCPublicationEventPropertyTypes, SCThing>>(false);
assert<Extends<SCPublicationEventWithoutReferences, SCThing>>(false);
assert<Extends<SCPublicationEvent, SCThing>>(true);

/**
 * Types of properties of SCRoom
 */
type SCRoomPropertyTypes = PropertyTypesNested<SCRoom>;
assert<NotHas<SCRoomPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCRoomPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCRoomPropertyTypes, SCThing>>(true);
assert<Has<SCRoomPropertyTypes, SCThing>>(false);
assert<Extends<SCRoomWithoutReferences, SCThing>>(false);
assert<Extends<SCRoom, SCThing>>(true);

/**
 * Types of properties of SCSemester
 */
type SCSemesterPropertyTypes = PropertyTypesNested<SCSemester>;
assert<NotHas<SCSemesterPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCSemesterPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCSemesterPropertyTypes, SCThing>>(true);
assert<Has<SCSemesterPropertyTypes, SCThing>>(false);
assert<Extends<SCSemesterWithoutReferences, SCThing>>(false);
assert<Extends<SCSemester, SCThing>>(true);

/**
 * Types of properties of SCSetting
 */
type SCSettingPropertyTypes = PropertyTypesNested<SCSetting>;
assert<NotHas<SCSettingPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCSettingPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCSettingPropertyTypes, SCThing>>(true);
assert<Has<SCSettingPropertyTypes, SCThing>>(false);
assert<Extends<SCSettingWithoutReferences, SCThing>>(false);
assert<Extends<SCSetting, SCThing>>(true);

/**
 * Types of properties of SCSportCourse
 */
type SCSportCoursePropertyTypes = PropertyTypesNested<SCSportCourse>;
assert<NotHas<SCSportCoursePropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCSportCoursePropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCSportCoursePropertyTypes, SCThing>>(true);
assert<Has<SCSportCoursePropertyTypes, SCThing>>(false);
assert<Extends<SCSportCourseWithoutReferences, SCThing>>(false);
assert<Extends<SCSportCourse, SCThing>>(true);

/**
 * Types of properties of SCStudyModule
 */
type SCStudyModulePropertyTypes = PropertyTypesNested<SCStudyModule>;
assert<NotHas<SCStudyModulePropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCStudyModulePropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCStudyModulePropertyTypes, SCThing>>(true);
assert<Has<SCStudyModulePropertyTypes, SCThing>>(false);
assert<Extends<SCStudyModuleWithoutReferences, SCThing>>(false);
assert<Extends<SCStudyModule, SCThing>>(true);

/**
 * Types of properties of SCTicket
 */
type SCTicketPropertyTypes = PropertyTypesNested<SCTicket>;
assert<NotHas<SCTicketPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCTicketPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCTicketPropertyTypes, SCThing>>(true);
assert<Has<SCTicketPropertyTypes, SCThing>>(false);
assert<Extends<SCTicketWithoutReferences, SCThing>>(false);
assert<Extends<SCTicket, SCThing>>(true);

/**
 * Types of properties of SCToDo
 */
type SCToDoPropertyTypes = PropertyTypesNested<SCToDo>;
assert<NotHas<SCToDoPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCToDoPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCToDoPropertyTypes, SCThing>>(true);
assert<Has<SCToDoPropertyTypes, SCThing>>(false);
assert<Extends<SCToDoWithoutReferences, SCThing>>(false);
assert<Extends<SCToDo, SCThing>>(true);

/**
 * Types of properties of SCToDo
 */
type SCTourPropertyTypes = PropertyTypesNested<SCTour>;
assert<NotHas<SCTourPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCTourPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCTourPropertyTypes, SCThing>>(true);
assert<Has<SCTourPropertyTypes, SCThing>>(false);
assert<Extends<SCTourWithoutReferences, SCThing>>(false);
assert<Extends<SCTour, SCThing>>(true);

/**
 * Types of properties of SCVideo
 */
type SCVideoPropertyTypes = PropertyTypesNested<SCVideo>;
assert<NotHas<SCVideoPropertyTypes, SCThingWithoutReferences>>(false);
assert<Has<SCVideoPropertyTypes, SCThingWithoutReferences>>(true);
assert<NotHas<SCVideoPropertyTypes, SCThing>>(true);
assert<Has<SCVideoPropertyTypes, SCThing>>(false);
assert<Extends<SCVideoWithoutReferences, SCThing>>(false);
assert<Extends<SCVideo, SCThing>>(true);

/**
 * Dummy interface, to check if union types still resolve to any if one of the members is any
 */
interface Foo {
  bar: SCPerson;
  foo: any;
}

/**
 * Type that is unfortunately never - blame TypeScript
 */
type UnfortunatelyNever = PropertyTypesNested<Foo>;
assert<IsNever<UnfortunatelyNever>>(true);

/**
 * Flat property types
 */
type FlatPropertyTypes<T> = T[keyof T];

/**
 * Type that is unfortunately any - blame TypeScript
 */
type UnfortunatelyAny = FlatPropertyTypes<Foo>;
assert<IsAny<UnfortunatelyAny>>(true);
