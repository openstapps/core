import {validateFiles, writeReport} from '@openstapps/core-tools/lib/validate';
import {slow, suite, test, timeout} from '@testdeck/mocha';
import {expect} from 'chai';
import {mkdirSync} from 'fs';
import {join, resolve} from 'path';

@suite(timeout(15000), slow(10000))
export class SchemaSpec {
  @test
  async 'validate against test files'() {
    const errorsPerFile = {
      ...(await validateFiles(resolve('lib', 'schema'), resolve('test', 'resources'))),
      ...(await validateFiles(resolve('lib', 'schema'), resolve('test', 'resources', 'indexable'))),
    };

    let unexpected = false;
    Object.keys(errorsPerFile).forEach(file => {
      unexpected = unexpected || errorsPerFile[file].some(error => !error.expected);
    });

    mkdirSync('report', {
      recursive: true,
    });

    await writeReport(join('report', 'index.html'), errorsPerFile);

    expect(unexpected).to.be.equal(false);
  }
}
