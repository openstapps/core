/*
 * Copyright (C) 2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {lightweightProjectFromPath} from '@openstapps/core-tools/lib/easy-ast/easy-ast';
import {LightweightProject} from '@openstapps/core-tools/lib/easy-ast/types/lightweight-project';
import {expect} from 'chai';
import {reduce} from 'lodash';

process.on('unhandledRejection', err => {
  throw err;
});

describe('Mapping Compatibility', () => {
  let project: LightweightProject;

  before(function () {
    this.timeout(15000);
    this.slow(10000);

    project = lightweightProjectFromPath('src');
  });

  it('non-exported definitions should not have duplicate names across files', () => {
    reduce(
      project,
      (result, file) =>
        reduce(
          file,
          (result2, _, key) => {
            expect(result2[key]).to.be.undefined;
            return {
              [key]: true,
              ...result2,
            };
          },
          result,
        ),
      {} as Record<string, boolean>,
    );
  });
});
