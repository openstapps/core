/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {slow, suite, test, timeout} from '@testdeck/mocha';
import {expect} from 'chai';
import {
  isBulkResponse,
  isMultiSearchResponse,
  isSearchResponse,
  isThing,
  isThingWithTranslations,
} from '../src/guards';
import {SCBulkResponse} from '../src/protocol/routes/bulk-request';
import {SCSearchResponse} from '../src/protocol/routes/search';
import {SCMultiSearchResponse} from '../src/protocol/routes/search-multi';
import {SCThingOriginType, SCThingType} from '../src/things/abstract/thing';
import {SCDish} from '../src/things/dish';

@suite(timeout(10000), slow(5000))
export class GuardsSpec {
  static bulkResponse: SCBulkResponse = {
    expiration: '2009-06-30T18:30:00+02:00 ',
    source: 'bar',
    state: 'done',
    type: SCThingType.Dish,
    uid: 'foo',
  };

  static dishWithTranslation: SCDish = {
    categories: ['appetizer'],
    name: 'foo',
    origin: {
      created: '',
      type: SCThingOriginType.User,
    },
    translations: {
      de: {
        name: 'Foo',
      },
    },
    type: SCThingType.Dish,
    uid: 'bar',
  };

  static notADish = {
    categories: ['appetizer'],
    name: 'foo',
    origin: {
      created: '',
      type: SCThingOriginType.User,
    },
    type: 'foobar',
    uid: 'bar',
  };

  static searchResponse: SCSearchResponse = {
    data: [GuardsSpec.dishWithTranslation],
    facets: [
      {
        buckets: [
          {
            count: 1,
            key: 'key',
          },
        ],
        field: 'field',
      },
    ],
    pagination: {
      count: 1,
      offset: 0,
      total: 1,
    },
    stats: {
      time: 1,
    },
  };

  @test
  public isBulkResponse() {
    expect(isBulkResponse(null)).to.be.equal(false);
    expect(isBulkResponse(GuardsSpec.dishWithTranslation)).to.be.equal(false);
    expect(isBulkResponse(GuardsSpec.bulkResponse)).to.be.equal(true);
  }

  @test
  public isMultiSearchResponse() {
    const multiSearchResponse: SCMultiSearchResponse = {
      foo: GuardsSpec.searchResponse,
    };
    expect(isMultiSearchResponse(multiSearchResponse)).to.be.equal(true);
    const notAMultiSearchResponse = {...multiSearchResponse, ...{bar: 'baz'}};
    expect(isMultiSearchResponse(notAMultiSearchResponse)).to.be.equal(false);
    delete multiSearchResponse.foo;
    expect(isMultiSearchResponse(multiSearchResponse)).to.be.equal(false);
  }

  @test
  public isSearchResponse() {
    const notASearchResponse = {...GuardsSpec.searchResponse};
    // @ts-ignore
    delete notASearchResponse.pagination;
    expect(isSearchResponse(notASearchResponse)).to.be.equal(false);
    // @ts-ignore
    delete notASearchResponse.data;
    expect(isSearchResponse(notASearchResponse)).to.be.equal(false);
    expect(isSearchResponse(null)).to.be.equal(false);
    expect(isSearchResponse(GuardsSpec.searchResponse)).to.be.equal(true);
  }

  @test
  public isThing() {
    expect(isThing('foo')).to.be.equal(false);
    expect(isThing({type: 'foo'})).to.be.equal(false);
    expect(isThing(GuardsSpec.notADish)).to.be.equal(false);
    expect(isThing(GuardsSpec.dishWithTranslation)).to.be.equal(true);
  }

  @test
  public isThingWithTranslations() {
    const dishWithoutTranslation = {...GuardsSpec.dishWithTranslation};
    delete dishWithoutTranslation.translations;
    expect(isThingWithTranslations(dishWithoutTranslation)).to.be.equal(false);
    expect(isThingWithTranslations(GuardsSpec.dishWithTranslation)).to.be.equal(true);
  }
}
