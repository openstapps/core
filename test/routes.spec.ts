/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {slow, suite, test, timeout} from '@testdeck/mocha';
import {expect} from 'chai';
import {SCBulkAddRoute} from '../src/protocol/routes/bulk-add';
import {SCBulkRoute} from '../src/protocol/routes/bulk-request';
import {SCThingUpdateRoute} from '../src/protocol/routes/thing-update';

@suite(timeout(10000), slow(5000))
export class RoutesSpec {
  @test
  public bulkAddRouteUrlPath() {
    const bulkAddRoute = new SCBulkAddRoute();

    expect(
      bulkAddRoute.getUrlPath({
        UID: '540862f3-ea30-5b8f-8678-56b4dc217140',
      }),
    ).to.equal('/bulk/540862f3-ea30-5b8f-8678-56b4dc217140');
  }

  @test
  public bulkRouteUrlPath() {
    const bulkRoute = new SCBulkRoute();

    expect(bulkRoute.getUrlPath()).to.equal('/bulk');
  }

  @test
  public thingUpdateRouteUrlPath() {
    const thingUpdateRoute = new SCThingUpdateRoute();

    expect(
      thingUpdateRoute.getUrlPath({
        TYPE: 'dish',
        UID: '540862f3-ea30-5b8f-8678-56b4dc217140',
      }),
    ).to.equal('/dish/540862f3-ea30-5b8f-8678-56b4dc217140');
  }

  @test
  public tooManyParameters() {
    const thingUpdateRoute = new SCThingUpdateRoute();

    const fn = () => {
      thingUpdateRoute.getUrlPath({
        FOO: 'bar',
        TYPE: 'dish',
        UID: '540862f3-ea30-5b8f-8678-56b4dc217140',
      });
    };

    expect(fn).to.throw('Extraneous parameters provided.');
  }

  @test
  public wrongParameters() {
    const thingUpdateRoute = new SCThingUpdateRoute();

    const fn = () => {
      thingUpdateRoute.getUrlPath({
        TYPO: 'dish',
        UID: '540862f3-ea30-5b8f-8678-56b4dc217140',
      });
    };

    expect(fn).to.throw("Parameter 'TYPE' not provided.");
  }
}
