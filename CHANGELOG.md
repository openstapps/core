# [1.1.0](https://gitlab.com/openstapps/core/compare/v1.0.1...v1.1.0) (2023-05-19)


### Features

* add certification thing ([fd63fb7](https://gitlab.com/openstapps/core/commit/fd63fb764f882a87b8da3c5fb27701a090469df2))



## [1.0.1](https://gitlab.com/openstapps/core/compare/v1.0.0...v1.0.1) (2023-04-28)



# [1.0.0](https://gitlab.com/openstapps/core/compare/v0.75.0...v1.0.0) (2023-04-28)


### Features

* support ES 8.4 via mapping generator ([afcc73f](https://gitlab.com/openstapps/core/commit/afcc73f3da856a76a613d939a759cf1cd701a2c5))



# [0.75.0](https://gitlab.com/openstapps/core/compare/v0.74.0...v0.75.0) (2023-02-28)


### Features

* add rating for things ([28eacf7](https://gitlab.com/openstapps/core/commit/28eacf7925f84caa129cad4b94fb449effd4d6ea)), closes [#152](https://gitlab.com/openstapps/core/issues/152)



# [0.74.0](https://gitlab.com/openstapps/core/compare/v0.73.0...v0.74.0) (2023-01-30)



# [0.73.0](https://gitlab.com/openstapps/core/compare/v0.72.0...v0.73.0) (2023-01-12)



# [0.72.0](https://gitlab.com/openstapps/core/compare/v0.71.1...v0.72.0) (2022-12-06)


### Features

* add dish menu section and service times ([be98fd8](https://gitlab.com/openstapps/core/commit/be98fd8c4c2fbb01eb80808bf3aa609b08b90ec6)), closes [#150](https://gitlab.com/openstapps/core/issues/150)



## [0.71.1](https://gitlab.com/openstapps/core/compare/v0.71.0...v0.71.1) (2022-11-22)



# [0.71.0](https://gitlab.com/openstapps/core/compare/v0.70.0...v0.71.0) (2022-10-11)


### Features

* add new book categories ([dd138fd](https://gitlab.com/openstapps/core/commit/dd138fd0be6d8100fc242816f5eb2549f2672ab3)), closes [#147](https://gitlab.com/openstapps/core/issues/147)



# [0.70.0](https://gitlab.com/openstapps/core/compare/v0.69.0...v0.70.0) (2022-09-02)


### Features

* add auth provider to config menu items ([b183198](https://gitlab.com/openstapps/core/commit/b18319802969d711373e70cca5921730345aa3e8))



# [0.69.0](https://gitlab.com/openstapps/core/compare/v0.68.0...v0.69.0) (2022-08-17)



# [0.68.0](https://gitlab.com/openstapps/core/compare/v0.67.0...v0.68.0) (2022-06-27)



# [0.67.0](https://gitlab.com/openstapps/core/compare/v0.66.1...v0.67.0) (2022-05-31)



## [0.66.1](https://gitlab.com/openstapps/core/compare/v0.66.0...v0.66.1) (2022-05-27)



# [0.66.0](https://gitlab.com/openstapps/core/compare/v0.65.1...v0.66.0) (2022-05-11)


### Features

* add geo filter envelope support ([484be6a](https://gitlab.com/openstapps/core/commit/484be6a890d743601efa5d40d33ea2c619f3126d))



## [0.65.1](https://gitlab.com/openstapps/core/compare/v0.65.0...v0.65.1) (2022-04-04)



# [0.65.0](https://gitlab.com/openstapps/core/compare/v0.64.0...v0.65.0) (2022-04-04)



# [0.64.0](https://gitlab.com/openstapps/core/compare/v0.63.0...v0.64.0) (2022-03-21)



# [0.63.0](https://gitlab.com/openstapps/core/compare/v0.62.0...v0.63.0) (2022-01-24)


### Bug Fixes

* add auth object to index route ([e149931](https://gitlab.com/openstapps/core/commit/e14993114f0ec370775010eee4ad0d302a0beebb))



# [0.62.0](https://gitlab.com/openstapps/core/compare/v0.61.0...v0.62.0) (2022-01-21)


### Bug Fixes

* add missing lastPublished to SCCreativeWork ([e0adb23](https://gitlab.com/openstapps/core/commit/e0adb2332ec24ce388c4c008b51b7aa941948532))



# [0.61.0](https://gitlab.com/openstapps/core/compare/v0.60.0...v0.61.0) (2022-01-21)



# [0.60.0](https://gitlab.com/openstapps/core/compare/v0.59.0...v0.60.0) (2022-01-21)



# [0.59.0](https://gitlab.com/openstapps/core/compare/v0.58.0...v0.59.0) (2022-01-20)



# [0.58.0](https://gitlab.com/openstapps/core/compare/v0.57.0...v0.58.0) (2022-01-18)



# [0.57.0](https://gitlab.com/openstapps/core/compare/v0.56.0...v0.57.0) (2022-01-18)


### Features

* extend config to describe auth providers ([7553620](https://gitlab.com/openstapps/core/commit/7553620a5d330ebfb66461afeab700e36bd37165))



# [0.56.0](https://gitlab.com/openstapps/core/compare/v0.55.0...v0.56.0) (2021-12-17)



# [0.55.0](https://gitlab.com/openstapps/core/compare/v0.54.0...v0.55.0) (2021-12-15)



# [0.54.0](https://gitlab.com/openstapps/core/compare/v0.53.0...v0.54.0) (2021-11-17)


### Features

* add SCAssessment ([7a2e0f2](https://gitlab.com/openstapps/core/commit/7a2e0f20d1c64ab1deb7ab30bfb4bab4daaabd6d))



# [0.53.0](https://gitlab.com/openstapps/core/compare/v0.52.0...v0.53.0) (2021-10-19)



# [0.52.0](https://gitlab.com/openstapps/core/compare/v0.51.0...v0.52.0) (2021-09-28)


### Features

* add about config ([aa294c4](https://gitlab.com/openstapps/core/commit/aa294c4e29e9191bef6d79487b0b321fbc34f6fb))



# [0.51.0](https://gitlab.com/openstapps/core/compare/v0.50.0...v0.51.0) (2021-09-10)


### Bug Fixes

* add physicalobject to book categories ([ded8e7d](https://gitlab.com/openstapps/core/commit/ded8e7dfd51094c02a86e1383a4e94c069c10e64))



# [0.50.0](https://gitlab.com/openstapps/core/compare/v0.49.5...v0.50.0) (2021-09-01)



## [0.49.5](https://gitlab.com/openstapps/core/compare/v0.49.3...v0.49.5) (2021-08-27)


### Bug Fixes

* test resources from hds2 ([dfe35d7](https://gitlab.com/openstapps/core/commit/dfe35d71a38c35064726365f99714abff3b30ba6))



## [0.49.3](https://gitlab.com/openstapps/core/compare/v0.49.2...v0.49.3) (2021-08-18)



## [0.49.2](https://gitlab.com/openstapps/core/compare/v0.49.1...v0.49.2) (2021-08-17)



## [0.49.1](https://gitlab.com/openstapps/core/compare/v0.49.0...v0.49.1) (2021-08-13)


### Bug Fixes

* documentation generation ([8a28d6f](https://gitlab.com/openstapps/core/commit/8a28d6fa8657d778d6ae0d38cda7da3531d6478c))



# [0.49.0](https://gitlab.com/openstapps/core/compare/v0.48.0...v0.49.0) (2021-08-12)


### Features

* add elasticsearch mappings to build ([21eeecd](https://gitlab.com/openstapps/core/commit/21eeecd5ee0d68a4faa93bb70d2187ce35807b01))



# [0.48.0](https://gitlab.com/openstapps/core/compare/v0.47.0...v0.48.0) (2021-07-14)


### Bug Fixes

* correct parameter name of thing update route ([872c1f5](https://gitlab.com/openstapps/core/commit/872c1f5fc3884b6df6f5025287873d98b27dd6b1))


### Features

* support geo shape queries ([882483e](https://gitlab.com/openstapps/core/commit/882483ee2490938014904bc26687bd1648992ae6))



# [0.47.0](https://gitlab.com/openstapps/core/compare/v0.46.0...v0.47.0) (2021-06-15)


### Bug Fixes

* set date-series frequency non translatable ([4a4cd4e](https://gitlab.com/openstapps/core/commit/4a4cd4e6d4b01ac34b5338dfcf4aeb959f54461d))


### Features

* make SCDateSeries.frequency compliant with schema.org ([942cd11](https://gitlab.com/openstapps/core/commit/942cd1146966aa9e60321160489a6605877925e9))



# [0.46.0](https://gitlab.com/openstapps/core/compare/v0.45.0...v0.46.0) (2021-05-04)


### Features

* change range offer to use date range ([9c6972a](https://gitlab.com/openstapps/core/commit/9c6972af787678567409877b6bc9f9417e1374e2))



# [0.45.0](https://gitlab.com/openstapps/core/compare/v0.44.0...v0.45.0) (2021-04-26)



# [0.44.0](https://gitlab.com/openstapps/core/compare/v0.43.0...v0.44.0) (2021-04-12)


### Features

* add support for multiple values in the value filter ([913c569](https://gitlab.com/openstapps/core/commit/913c5691da46ad2591fe14bcf7286f12b29ab5ee))



# [0.43.0](https://gitlab.com/openstapps/core/compare/v0.42.0...v0.43.0) (2021-04-07)



# [0.42.0](https://gitlab.com/openstapps/core/compare/v0.41.0...v0.42.0) (2021-02-16)



# [0.41.0](https://gitlab.com/openstapps/core/compare/v0.40.0...v0.41.0) (2021-02-08)


### Bug Fixes

* add date, numeric range filter to SCSearchFilter ([8510f11](https://gitlab.com/openstapps/core/commit/8510f11d7b4c62a6b239a70f47fe07e8cc86ab63))



# [0.40.0](https://gitlab.com/openstapps/core/compare/v0.39.0...v0.40.0) (2021-02-05)


### Features

* add range filter, date sorting support ([29bc006](https://gitlab.com/openstapps/core/commit/29bc00616e87a8d346d8c304fab2e3818921c75e))



# [0.39.0](https://gitlab.com/openstapps/core/compare/v0.38.1...v0.39.0) (2020-12-01)


### Features

* extend property value translation retrival ([a246bde](https://gitlab.com/openstapps/core/commit/a246bdea84e0ca390be6ab38723d637626db87d2))



## [0.38.1](https://gitlab.com/openstapps/core/compare/v0.38.0...v0.38.1) (2020-11-02)



# [0.38.0](https://gitlab.com/openstapps/core/compare/v0.37.0...v0.38.0) (2020-10-30)



# [0.37.0](https://gitlab.com/openstapps/core/compare/v0.36.0...v0.37.0) (2020-09-23)


### Bug Fixes

* remove keyword tag from steps ([c369c85](https://gitlab.com/openstapps/core/commit/c369c8520a2eed169555a35a50ce745c08e1f9da))
* remove redundant property declaration ([66075ef](https://gitlab.com/openstapps/core/commit/66075ef99b95198a9cd5c0a396603e089221bcd9))



# [0.36.0](https://gitlab.com/openstapps/core/compare/v0.35.0...v0.36.0) (2020-07-16)


### Features

* add function to get translated SCThingType ([fe7f1a5](https://gitlab.com/openstapps/core/commit/fe7f1a53ae46e052e23f1b39851f1547b5a8dded))



# [0.35.0](https://gitlab.com/openstapps/core/compare/v0.34.0...v0.35.0) (2020-05-13)



# [0.34.0](https://gitlab.com/openstapps/core/compare/v0.33.0...v0.34.0) (2020-04-21)


### Features

* add [@sortable](https://gitlab.com/sortable) tags to certain translatable properties ([f5e8856](https://gitlab.com/openstapps/core/commit/f5e88569eb75578febbcde67259c0c14563e53fe))
* annotate SCThing uid and url as filterable ([70c1a3e](https://gitlab.com/openstapps/core/commit/70c1a3eaa3d1c88f4b86f0df86d0d362ad1f930c))
* Update src/things/book.ts - made ISBN optional ([6060113](https://gitlab.com/openstapps/core/commit/6060113df56b871bb5014a8a961974895e52158f))



# [0.33.0](https://gitlab.com/openstapps/core/compare/v0.32.0...v0.33.0) (2020-02-11)



# [0.32.0](https://gitlab.com/openstapps/core/compare/v0.31.0...v0.32.0) (2020-02-04)


### Features

* make SCContactPoint extend SCThing ([0d89b14](https://gitlab.com/openstapps/core/commit/0d89b1493293f83e9096615f653a6094519d59f2))



# [0.31.0](https://gitlab.com/openstapps/core/compare/v0.30.0...v0.31.0) (2019-11-14)


### Bug Fixes

* remove categories from custom translations ([9658f05](https://gitlab.com/openstapps/core/commit/9658f05d31366b3735da3aa548ef5ed0255d8054))
* translator can now handle enum translations ([abda5cf](https://gitlab.com/openstapps/core/commit/abda5cf0caead37f085431e1e5a9771b79272ec6))



# [0.30.0](https://gitlab.com/openstapps/core/compare/v0.29.0...v0.30.0) (2019-11-08)


### Features

* add aggregatable tag for type field ([443cb74](https://gitlab.com/openstapps/core/commit/443cb748fba1575f5f1e16c550fd33eb7fa7901c))
* add new field sequenceIndex to message ([01f92ba](https://gitlab.com/openstapps/core/commit/01f92baa985013bfe1c79fad60351fdaf44bd676))



# [0.29.0](https://gitlab.com/openstapps/core/compare/v0.28.0...v0.29.0) (2019-09-17)


### Features

* add mappingIgnoredTags property to SCBackend ([149f3ff](https://gitlab.com/openstapps/core/commit/149f3ffff15dce27337665abba520d11bc3014dd))



# [0.28.0](https://gitlab.com/openstapps/core/compare/v0.27.0...v0.28.0) (2019-09-10)


### Features

* add onlyOnType field for SCFacet ([fba63db](https://gitlab.com/openstapps/core/commit/fba63db137c52212113545be1062d760f0c4213f))



# [0.27.0](https://gitlab.com/openstapps/core/compare/v0.26.0...v0.27.0) (2019-09-03)


### Features

* add [@filterable](https://gitlab.com/filterable) tags ([fec3371](https://gitlab.com/openstapps/core/commit/fec33715add996ae0a7125ad00ee043d288c6671))



# [0.26.0](https://gitlab.com/openstapps/core/compare/v0.25.0...v0.26.0) (2019-08-19)


### Bug Fixes

* complete german translations ([bad15dc](https://gitlab.com/openstapps/core/commit/bad15dc8728954ee6ee23fa68c471647328830b9))


### Features

* add pluginRequestTimeout field to backend config ([88f579e](https://gitlab.com/openstapps/core/commit/88f579e09bc5e7726dbc4fc788737ba49a0801cd))



# [0.25.0](https://gitlab.com/openstapps/core/compare/v0.24.0...v0.25.0) (2019-07-25)


### Bug Fixes

* correct package job ([d4a8ef6](https://gitlab.com/openstapps/core/commit/d4a8ef6a9ca69849f8b981804da45e4ba907f37b))



# [0.24.0](https://gitlab.com/openstapps/core/compare/v0.23.1...v0.24.0) (2019-07-23)


### Features

* add new field receivingOrganisations to message ([d781dd6](https://gitlab.com/openstapps/core/commit/d781dd6de5cc7572b9218df959247b93cb2db305))



## [0.23.1](https://gitlab.com/openstapps/core/compare/v0.23.0...v0.23.1) (2019-07-15)


### Bug Fixes

* use correct generic for the distance filter ([f262a6b](https://gitlab.com/openstapps/core/commit/f262a6b8fa7a9d40234569ec490d5ef8e9437a75))


### Features

* add type maps from routes to requests and responses ([e1d4b2b](https://gitlab.com/openstapps/core/commit/e1d4b2bc9de60a2332788c9a2264760d151c8813)), closes [#80](https://gitlab.com/openstapps/core/issues/80)



# [0.23.0](https://gitlab.com/openstapps/core/compare/v0.22.0...v0.23.0) (2019-07-03)


### Features

* add mapping type from response to request and vice versa ([c18e3c1](https://gitlab.com/openstapps/core/commit/c18e3c15f1940338b6872343f6b20dba6346f84b)), closes [#80](https://gitlab.com/openstapps/core/issues/80)
* support 404 (resource not found) error on plugin register route ([43851d2](https://gitlab.com/openstapps/core/commit/43851d2d358c4c342a80a863b059a6447c819c04))



# [0.22.0](https://gitlab.com/openstapps/core/compare/v0.21.0...v0.22.0) (2019-06-28)



# [0.21.0](https://gitlab.com/openstapps/core/compare/v0.20.0...v0.21.0) (2019-06-19)


### Features

* add ES annotations ([9c424b0](https://gitlab.com/openstapps/core/commit/9c424b0f96da26d3aae9ffaadb2c640e7369ca7e))
* add static typed test for consistency ([ff1f554](https://gitlab.com/openstapps/core/commit/ff1f554e0beacb5ce2f3308c8212e2b8e4d8cbb0)), closes [#71](https://gitlab.com/openstapps/core/issues/71)



# [0.20.0](https://gitlab.com/openstapps/core/compare/v0.19.0...v0.20.0) (2019-06-11)


### Bug Fixes

* add translations for every SCThing ([f847a2a](https://gitlab.com/openstapps/core/commit/f847a2aa0cf576e2fd79197cdc7795638d929136))
* rename properties floor and message so that they are not identical to SCThingTypes ([78b64ba](https://gitlab.com/openstapps/core/commit/78b64bae08d9ca12badaedf921a24fa5ffe90552))



# [0.19.0](https://gitlab.com/openstapps/core/compare/v0.18.0...v0.19.0) (2019-05-17)


### Bug Fixes

* add thing without references, split thing ([da0507e](https://gitlab.com/openstapps/core/commit/da0507ee341af75cd15dddeeb3f4cbe08899434c)), closes [#69](https://gitlab.com/openstapps/core/issues/69)
* adjust model to remove references from things without references ([ca72c20](https://gitlab.com/openstapps/core/commit/ca72c20bd098032745ed0eb6978902b6ff6feac5)), closes [#69](https://gitlab.com/openstapps/core/issues/69)
* remove references from origins ([1d6a2b7](https://gitlab.com/openstapps/core/commit/1d6a2b7841e9c38e68c9e5c718377a9c04fe50d6)), closes [#69](https://gitlab.com/openstapps/core/issues/69)



# [0.18.0](https://gitlab.com/openstapps/core/compare/v0.17.0...v0.18.0) (2019-05-14)


### Features

* add study module interface ([d3790ad](https://gitlab.com/openstapps/core/commit/d3790adbd82417923d79b4a5a35c74fcaab3e050))



# [0.17.0](https://gitlab.com/openstapps/core/compare/v0.16.0...v0.17.0) (2019-04-16)



# [0.16.0](https://gitlab.com/openstapps/core/compare/v0.15.0...v0.16.0) (2019-04-15)



# [0.15.0](https://gitlab.com/openstapps/core/compare/v0.14.0...v0.15.0) (2019-04-09)


### Bug Fixes

* change SCThingMeta getInstance() return value ([4986042](https://gitlab.com/openstapps/core/commit/4986042428e24a04b73fb03d71c01353588419a7))
* resolve issues with things that can be offered ([623ed61](https://gitlab.com/openstapps/core/commit/623ed613a9405b53a86a33c13d48bc49d48de1ce)), closes [#41](https://gitlab.com/openstapps/core/issues/41)


### Features

* provide context based search ([3242411](https://gitlab.com/openstapps/core/commit/324241176849b338bee9f5d1dcaf1109f47a8bb9))



# [0.14.0](https://gitlab.com/openstapps/core/compare/v0.13.0...v0.14.0) (2019-04-03)


### Features

* add model for plugin register route ([8188731](https://gitlab.com/openstapps/core/commit/81887315f8c3038ec37c2197db8e193da835f6fc))



# [0.13.0](https://gitlab.com/openstapps/core/compare/v0.12.0...v0.13.0) (2019-04-02)


### Bug Fixes

* correct isThing guard ([67868e9](https://gitlab.com/openstapps/core/commit/67868e9eb810de94ca0bb6e1298799ee3f550145))
* update tslint  dependencies ([bbe4fca](https://gitlab.com/openstapps/core/commit/bbe4fcac42c21c68b5c6aa6bcf9bbdecfdf84de7))


### Features

* add conditional "maps" for associated types ([c8bda2e](https://gitlab.com/openstapps/core/commit/c8bda2eae7f44ec4a284cc26fc85051a9a45e192)), closes [#50](https://gitlab.com/openstapps/core/issues/50)
* provide sample JSON files with the package ([5d1e79d](https://gitlab.com/openstapps/core/commit/5d1e79d4879e6ce8f43d4cebb9d8c8500d5d16b7)), closes [#46](https://gitlab.com/openstapps/core/issues/46)



# [0.12.0](https://gitlab.com/openstapps/core/compare/v0.11.0...v0.12.0) (2019-03-14)


### Bug Fixes

* add todo to SCThingsWithoutDiff and SCClasses ([9a49442](https://gitlab.com/openstapps/core/commit/9a49442902a9f49439083a9197bd9f0a231d007d)), closes [#39](https://gitlab.com/openstapps/core/issues/39)


### Features

* add SCThingTranslator class. move functionality accordingly ([90e3d22](https://gitlab.com/openstapps/core/commit/90e3d2239921bbfcea27b64aef9344ffee270b6d))



# [0.11.0](https://gitlab.com/openstapps/core/compare/v0.10.0...v0.11.0) (2019-02-21)


### Features

* add laboratory and computer as room categories ([a0ab72e](https://gitlab.com/openstapps/core/commit/a0ab72e597f6a14cfc0bd8935e1ef765b86b90f7)), closes [#33](https://gitlab.com/openstapps/core/issues/33)
* add maxRequestBodySize in backend configuration ([b5bd09e](https://gitlab.com/openstapps/core/commit/b5bd09e40bfb49463815538ebaa44ff53b5d7b92))



# [0.10.0](https://gitlab.com/openstapps/core/compare/v0.9.0...v0.10.0) (2019-02-18)


### Features

* add model for requestBodyTooLargeError ([bc3a0f6](https://gitlab.com/openstapps/core/commit/bc3a0f6d11cba836a55cbd5787d68cfe655b68a6))



# [0.9.0](https://gitlab.com/openstapps/core/compare/v0.8.0...v0.9.0) (2019-02-14)



# [0.8.0](https://gitlab.com/openstapps/core/compare/v0.7.0...v0.8.0) (2019-02-13)


### Features

* add config for maximum queries ([c7ab473](https://gitlab.com/openstapps/core/commit/c7ab47397e2a33b1c460b9038cbf53fec148316d))



# [0.7.0](https://gitlab.com/openstapps/core/compare/v0.6.0...v0.7.0) (2019-02-13)



# [0.6.0](https://gitlab.com/openstapps/core/compare/v0.5.0...v0.6.0) (2019-02-07)



# [0.5.0](https://gitlab.com/openstapps/core/compare/v0.4.0...v0.5.0) (2019-02-06)


### Features

* add model for syntax error ([a3f9fcb](https://gitlab.com/openstapps/core/commit/a3f9fcbfb0c35206adabc95792de076518f604f7))



# [0.4.0](https://gitlab.com/openstapps/core/compare/v0.3.0...v0.4.0) (2019-01-31)


### Features

* add draft of todo ([2860a11](https://gitlab.com/openstapps/core/commit/2860a11b618081ab88ecb1780a280cab37217a0e))



# [0.3.0](https://gitlab.com/openstapps/core/compare/v0.2.0...v0.3.0) (2019-01-25)


### Bug Fixes

* set larger v8 stack size ([d3d08e7](https://gitlab.com/openstapps/core/commit/d3d08e7735808d4eae5aab4b263e36047c608679))


### Features

* add different origin types: remote and user ([13a4965](https://gitlab.com/openstapps/core/commit/13a49650c4498f6edbe17c9de3598e0180b866da)), closes [#12](https://gitlab.com/openstapps/core/issues/12)
* add saveable thing for saving user/client data ([a4f3fab](https://gitlab.com/openstapps/core/commit/a4f3fab033ca18352c0119061f2d0e64252d4bbb)), closes [#12](https://gitlab.com/openstapps/core/issues/12)



# [0.2.0](https://gitlab.com/openstapps/core/compare/v0.1.0...v0.2.0) (2019-01-09)


### Features

* use tag [@validatable](https://gitlab.com/validatable) to mark schema types ([7f248ee](https://gitlab.com/openstapps/core/commit/7f248eea13c732a95c4ee8e1bc91afeee52e8c2d))



# [0.1.0](https://gitlab.com/openstapps/core/compare/v0.0.2...v0.1.0) (2018-12-17)


### Features

* add base scheme for academic degrees ([85c8fc4](https://gitlab.com/openstapps/core/commit/85c8fc49c14b0ce45cf0cfdaf46c1ebe43b23ec7))
* add schema for course of studies ([2d4a76a](https://gitlab.com/openstapps/core/commit/2d4a76a555603a395c81a7a92ce5d95ed04de9de))
* add tool to generate documentation for routes ([1a07df2](https://gitlab.com/openstapps/core/commit/1a07df2d676895264f43b0138645d7c13f60c9db))



## [0.0.2](https://gitlab.com/openstapps/core/compare/v0.0.1...v0.0.2) (2018-11-29)



## [0.0.1](https://gitlab.com/openstapps/core/compare/2d770dde4464dc4cf4cdc29927aa4e4382914148...v0.0.1) (2018-11-29)


### Features

* add core ([2d770dd](https://gitlab.com/openstapps/core/commit/2d770dde4464dc4cf4cdc29927aa4e4382914148))



