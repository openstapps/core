/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {StatusCodes} from 'http-status-codes';
import {SCError} from '../error';

/**
 * An error that is returned whenever there is an unexpected error while creating a plugin
 *
 * @validatable
 */
export class SCPluginRegisteringFailedErrorResponse extends SCError {
  /**
   * Create a PluginRegisteringFailedError
   *
   * @param message Describes what went wrong wile registering the plugin
   * @param stack Set to true if a stack trace should be created
   */
  constructor(message: string, stack?: boolean) {
    super('PluginRegisteringFailedError', message, StatusCodes.INTERNAL_SERVER_ERROR, stack);
  }
}
