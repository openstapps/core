/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {StatusCodes} from 'http-status-codes';
import {SCError} from '../error';
import {SCPluginMetaData} from '../routes/plugin-register';

/**
 * An error that is returned when a plugin with the same name is already registered, to prevent two copies of a plugin
 * running at the same time.
 * This usually indicates that there is more than one instance a plugin running.
 *
 * @validatable
 */
export class SCPluginAlreadyRegisteredErrorResponse extends SCError {
  /**
   * Meta data of a registered plugin, which is in a conflict with the plugin we want to register.
   * If the stack is disabled this is not set for security reasons
   */
  additionalData?: SCPluginMetaData;

  /**
   * Create a SCPluginAlreadyRegisteredError
   *
   * @param message Provide further information why an already registered plugin matches the one we want to register
   * @param plugin Provides meta data of a registered plugin, which is in a conflict with the plugin we want to register
   * @param stack Set to true if a stack trace should be created
   */
  constructor(message: string, plugin: SCPluginMetaData, stack = false) {
    super('SCPluginAlreadyRegisteredError', message, StatusCodes.CONFLICT, stack);
    if (stack) {
      this.additionalData = plugin;
    }
  }
}
