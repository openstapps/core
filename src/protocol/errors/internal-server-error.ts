/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {StatusCodes} from 'http-status-codes';
import {SCError} from '../error';

/**
 * An error that is returned, when an internal server error occurred
 *
 * @validatable
 */
export class SCInternalServerErrorResponse extends SCError {
  /**
   * Internal error that occurred. If the stack is disabled this error is not set for security reasons
   */
  additionalData?: Error;

  /**
   * Create a SCInternalServerErrorResponse
   *
   * @param error Internal server error
   * @param stack Set to true if a stack trace should be created
   * and the internal server error should be displayed to the client
   */
  constructor(error?: Error, stack = false) {
    super('InternalServerError', 'Internal server error', StatusCodes.BAD_GATEWAY, stack);

    if (stack) {
      this.additionalData = error;
    }
  }
}
