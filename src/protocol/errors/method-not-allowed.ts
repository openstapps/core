/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {StatusCodes} from 'http-status-codes';
import {SCError} from '../error';

/**
 * An error that is returned, when the used HTTP method is not allowed on the requested route
 *
 * @validatable
 */
export class SCMethodNotAllowedErrorResponse extends SCError {
  /**
   * Create a SCMethodNotAllowedErrorResponse
   *
   * @param stack Set to true if a stack trace should be created
   */
  constructor(stack?: boolean) {
    super(
      'MethodNotAllowedError',
      'HTTP method is not allowed on this route',
      StatusCodes.METHOD_NOT_ALLOWED,
      stack,
    );
  }
}
