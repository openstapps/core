/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ValidationError} from '@openstapps/core-tools/lib/types/validator';
import {StatusCodes} from 'http-status-codes';
import {SCError} from '../error';

/**
 * An error that is returned when the validation of a request fails
 *
 * @validatable
 */
export class SCValidationErrorResponse extends SCError {
  /**
   * List of validatation errors
   */
  additionalData: ValidationError[];

  /**
   * Create a SCValidationErrorResponse
   *
   * @param errors List of validation errors
   * @param stack Set to true if a stack trace should be created
   */
  constructor(errors: ValidationError[], stack?: boolean) {
    super('ValidationError', 'Validation of request failed', StatusCodes.BAD_REQUEST, stack);
    this.additionalData = errors;
  }
}
