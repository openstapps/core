/*
 * Copyright (C) 2021-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Polygon, Position} from 'geojson';
import {SCThingsField} from '../../../meta';
import {SCSearchAbstractFilter, SCSearchAbstractFilterArguments} from '../filter';

/**
 * A geo filter
 *
 * Filter for documents that are in relation to some geo data
 */
export interface SCGeoFilter extends SCSearchAbstractFilter<SCGeoFilterArguments> {
  /**
   * @see SCSearchAbstractFilter.type
   */
  type: 'geo';
}

/**
 * A rectangular geo shape, representing the top-left and bottom-right corners
 *
 * This is an extension of the Geojson type
 * http://geojson.org/geojson-spec.html
 */
export interface Envelope {
  /**
   * The top-left and bottom-right corners of the bounding box
   */
  coordinates: [Position, Position];

  /**
   * The type of the geometry
   */
  type: 'envelope';
}

/**
 * Arguments for filter instruction by geo data
 */
export interface SCGeoFilterArguments extends SCSearchAbstractFilterArguments {
  /**
   * Field on which to filter
   */
  field: SCThingsField;

  /**
   * Geo data to check up on
   */
  shape: Polygon | Envelope;

  /**
   * Spatial relation between the provided shape and the shape of the field.
   *
   * intersects - both shapes intersect (default)
   * disjoint   - both shapes don't intersect
   * within     - the search shape contains the field shape
   * contains   - the search shape is contained in the field shape
   */
  spatialRelation?: 'intersects' | 'disjoint' | 'within' | 'contains';
}
