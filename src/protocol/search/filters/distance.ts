/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Position} from 'geojson';
import {SCThingsField} from '../../../meta';
import {SCSearchAbstractFilter, SCSearchAbstractFilterArguments} from '../filter';

/**
 * A distance filter
 *
 * Filter for documents that are in the given distance of the given location
 */
export interface SCSearchDistanceFilter extends SCSearchAbstractFilter<SCDistanceFilterArguments> {
  /**
   * @see SCSearchAbstractFilter.type
   */
  type: 'distance';
}

/**
 * Additional arguments for distance filters
 */
export interface SCDistanceFilterArguments extends SCSearchAbstractFilterArguments {
  /**
   * Distance in meters to filter from given location
   */
  distance: number;

  /**
   * Field which contains the location you want to filter
   */
  field: SCThingsField;

  /**
   * Position to calculate the distance to
   */
  position: Position;
}
