/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCSearchAbstractFilter, SCSearchAbstractFilterArguments, SCSearchFilter} from '../filter';

/**
 * A boolean filter
 *
 * This filter can be used to combine multiple filters with boolean operations.
 */
export interface SCSearchBooleanFilter extends SCSearchAbstractFilter<SCBooleanFilterArguments> {
  /**
   * @see SCSearchAbstractFilter.type
   */
  type: 'boolean';
}

/**
 * Additional arguments for boolean filters
 */
export interface SCBooleanFilterArguments extends SCSearchAbstractFilterArguments {
  /**
   * A list of filter to apply the boolean operation to
   */
  filters: SCSearchFilter[];

  /**
   * Boolean operation to apply to the filters
   */
  operation: 'and' | 'not' | 'or';
}
