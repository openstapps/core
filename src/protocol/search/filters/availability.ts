/*
 * Copyright (C) 2019-2021 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCISO8601Date} from '../../../general/time';
import {SCThingsField} from '../../../meta';
import {SCSearchAbstractFilter, SCSearchAbstractFilterArguments} from '../filter';

/**
 * An availability filter
 *
 * Filter for documents where it cannot be safely determined that they are not available
 */
export interface SCSearchAvailabilityFilter extends SCSearchAbstractFilter<SCAvailabilityFilterArguments> {
  /**
   * @see SCSearchAbstractFilter.type
   */
  type: 'availability';
}

/**
 * Arguments for filter instruction by availability
 */
export interface SCAvailabilityFilterArguments extends SCSearchAbstractFilterArguments {
  /**
   * Field which marks availability range
   */
  field: SCThingsField;

  /**
   * If set, the provided time will apply to the full hour, day, week, etc.
   */
  scope?: 's' | 'm' | 'H' | 'd' | 'w' | 'M' | 'y';

  /**
   * Time to check. Defaults current time if not set
   */
  time?: SCISO8601Date;
}
