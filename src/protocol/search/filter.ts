/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMap} from '../../general/map';
/**
 * All available filter types
 */
import {SCSearchAvailabilityFilter} from './filters/availability';
import {SCSearchBooleanFilter} from './filters/boolean';
import {SCSearchDistanceFilter} from './filters/distance';
import {SCGeoFilter} from './filters/geo';
import {SCSearchDateRangeFilter, SCSearchNumericRangeFilter} from './filters/range';
import {SCSearchValueFilter} from './filters/value';

/**
 * Filter instruction types
 */
export type SCSearchFilterType =
  | 'availability'
  | 'boolean'
  | 'distance'
  | 'value'
  | 'date range'
  | 'numeric range'
  | 'geo';

/**
 * Structure of a filter instruction
 */
export interface SCSearchAbstractFilter<T extends SCSearchAbstractFilterArguments> {
  /**
   * Arguments of filter
   */
  arguments: T;

  /**
   * Type of filter
   */
  type: SCSearchFilterType;
}

/**
 * Arguments for the filter instruction
 */
export type SCSearchAbstractFilterArguments = SCMap<unknown>;

/**
 * Available filter instructions
 */
export type SCSearchFilter =
  | SCSearchAvailabilityFilter
  | SCSearchBooleanFilter
  | SCSearchDistanceFilter
  | SCSearchValueFilter
  | SCSearchNumericRangeFilter
  | SCSearchDateRangeFilter
  | SCGeoFilter;
