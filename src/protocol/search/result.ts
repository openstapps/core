/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThings} from '../../meta';
import {SCFacet} from './facet';

/**
 * A search response
 */
export interface SCSearchResult {
  /**
   * Data (any data object)
   */
  data: SCThings[];

  /**
   * Facets (aggregations over all matching data)
   */
  facets: SCFacet[];

  /**
   * Pagination information
   */
  pagination: SCSearchResultPagination;

  /**
   * Stats of the search engine
   */
  stats: SCSearchResultSearchEngineStats;
}

/**
 * Stores information about Pagination
 */
export interface SCSearchResultPagination {
  /**
   * Count of given data. Same as data.length
   */
  count: number;

  /**
   * Offset of data on all matching data. Given by [[SCSearchQuery.from]]
   */
  offset: number;

  /**
   * Number of total matching data
   */
  total: number;
}

/**
 * Statistics of search engine
 */
export interface SCSearchResultSearchEngineStats {
  /**
   * Response time of the search engine in ms
   */
  time: number;
}
