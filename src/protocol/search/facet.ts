/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThingsField} from '../../meta';
import {SCThingType} from '../../things/abstract/thing';

/**
 * A search facet
 */
export interface SCFacet {
  /**
   * Buckets for the aggregation
   */
  buckets: SCFacetBucket[];

  /**
   * Field of the aggregation
   */
  field: SCThingsField;

  /**
   * Type of the aggregation
   */
  onlyOnType?: SCThingType;
}

/**
 * A bucket of a facet
 */
export interface SCFacetBucket {
  /**
   * Count of matching search results
   */
  count: number;

  /**
   * Key of a bucket
   */
  key: string;
}
