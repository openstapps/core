/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * A generic error that can be returned by the backend if somethings fails during the processing of a request
 *
 * @validatable
 */
export interface SCErrorResponse extends Error {
  /**
   * Additional data that describes the error
   */
  additionalData?: unknown;

  /**
   * HTTP status code to return this error with
   */
  statusCode: number;
}

/**
 * An error that can be created by the backend during the processing of a request
 */
export abstract class SCError implements SCErrorResponse {
  /**
   * Call stack of the error
   */
  stack?: string;

  /**
   * Instatiate an SCError
   *
   * @param name Name of the error
   * @param message Message of the error
   * @param statusCode  HTTP status code to return this error with
   * @param stack Set to true if a stack trace should be created
   */
  constructor(public name: string, public message: string, public statusCode: number, stack = false) {
    // generate stacktrace if needed
    if (stack) {
      // eslint-disable-next-line unicorn/error-message
      this.stack = new Error().stack;
    }
  }
}
