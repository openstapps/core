/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {StatusCodes} from 'http-status-codes';
import {SCISO8601Date} from '../../general/time';
import {SCUuid} from '../../general/uuid';
import {SCThingType} from '../../things/abstract/thing';
import {SCInternalServerErrorResponse} from '../errors/internal-server-error';
import {SCMethodNotAllowedErrorResponse} from '../errors/method-not-allowed';
import {SCRequestBodyTooLargeErrorResponse} from '../errors/request-body-too-large';
import {SCSyntaxErrorResponse} from '../errors/syntax-error';
import {SCUnsupportedMediaTypeErrorResponse} from '../errors/unsupported-media-type';
import {SCValidationErrorResponse} from '../errors/validation';
import {SCAbstractRoute, SCRouteHttpVerbs} from '../route';

/**
 * A bulk request
 *
 * Parameters to be sent to request a new bulk.
 *
 * @validatable
 */
export type SCBulkRequest = SCBulkParameters;

/**
 * Parameters for a bulk
 */
export interface SCBulkParameters {
  /**
   * Expiration of bulk
   *
   * If the date is hit and the bulk is not done, it will be deleted and all data removed.
   * Defaults to one hour.
   */
  expiration?: SCISO8601Date;

  /**
   * Source of data for this bulk
   *
   * A short "description" of the source of the data to identify it inside the database.
   * A second bulk with the same source overrides the data of the first bulk once it is done.
   */
  source: string;

  /**
   * Type of things that are indexed in this bulk.
   *
   */
  type: SCThingType;
}

/**
 * Requested Bulk from backend
 *
 * @validatable
 */
export interface SCBulkResponse extends SCBulkParameters {
  /**
   * State of bulk
   *
   * The state is `in progress` while it accepts things to be added to the bulk.
   * The state is `done` once it is closed.
   */
  state: 'in progress' | 'done';

  /**
   * Universally unique identifier of the bulk
   */
  uid: SCUuid;
}

/**
 * Route for bulk creation
 */
export class SCBulkRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCBulkRequest';
    this.responseBodyName = 'SCBulkResponse';
    this.statusCodeSuccess = StatusCodes.OK;
    this.urlPath = '/bulk';
  }
}
