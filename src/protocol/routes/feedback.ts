/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {StatusCodes} from 'http-status-codes';
import {SCMessage} from '../../things/message';
import {SCInternalServerErrorResponse} from '../errors/internal-server-error';
import {SCMethodNotAllowedErrorResponse} from '../errors/method-not-allowed';
import {SCRequestBodyTooLargeErrorResponse} from '../errors/request-body-too-large';
import {SCSyntaxErrorResponse} from '../errors/syntax-error';
import {SCUnsupportedMediaTypeErrorResponse} from '../errors/unsupported-media-type';
import {SCValidationErrorResponse} from '../errors/validation';
import {SCAbstractRoute, SCRouteHttpVerbs} from '../route';

/**
 * User feedback
 *
 * @validatable
 */
export interface SCFeedbackRequest extends SCMessage {
  /**
   * Meta data that helps to understand the feedback
   */
  metaData?: SCFeedbackRequestMetaData;
}

/**
 * A response to a feedback request
 *
 * @validatable
 */
export interface SCFeedbackResponse {}

/**
 * Route for feedback submission
 */
export class SCFeedbackRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCFeedbackRequest';
    this.responseBodyName = 'SCFeedbackResponse';
    this.statusCodeSuccess = StatusCodes.OK;
    this.urlPath = '/feedback';
  }
}

/**
 * Request Meta Data
 */
export interface SCFeedbackRequestMetaData {
  /**
   * Whether or not the user enabled the debug mode
   */
  debug?: boolean;

  /**
   * Platform identifier
   */
  platform: string;

  /**
   * Scope/app state at feedback invocation
   */
  scope: unknown;

  /**
   * Whether or not the feedback is sendable
   */
  sendable?: boolean;

  /**
   * App state that feedback was invoked from
   */
  state: unknown;

  /**
   * User agent
   */
  userAgent: string;

  /**
   * StApps version string
   */
  version: string;
}
