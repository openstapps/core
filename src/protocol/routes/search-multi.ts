/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {StatusCodes} from 'http-status-codes';
import {SCMap} from '../../general/map';
import {SCInternalServerErrorResponse} from '../errors/internal-server-error';
import {SCMethodNotAllowedErrorResponse} from '../errors/method-not-allowed';
import {SCRequestBodyTooLargeErrorResponse} from '../errors/request-body-too-large';
import {SCSyntaxErrorResponse} from '../errors/syntax-error';
import {SCTooManyRequestsErrorResponse} from '../errors/too-many-requests';
import {SCUnsupportedMediaTypeErrorResponse} from '../errors/unsupported-media-type';
import {SCValidationErrorResponse} from '../errors/validation';
import {SCAbstractRoute, SCRouteHttpVerbs} from '../route';
import {SCSearchQuery} from '../search/query';
import {SCSearchResult} from '../search/result';

/**
 * A multi search request
 *
 * This is a map of [[SCSearchRequest]]s indexed by name.
 *
 * **CAUTION: This is limited to an amount of queries. Currently this limit is 5.**
 *
 * @validatable
 */
export type SCMultiSearchRequest = SCMap<SCSearchQuery>;

/**
 * A multi search response
 *
 * This is a map of [[SCSearchResponse]]s indexed by name
 *
 * @validatable
 */
export type SCMultiSearchResponse = SCMap<SCSearchResult>;

/**
 * Route for submission of multiple search requests at once
 */
export class SCMultiSearchRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
      SCTooManyRequestsErrorResponse,
      SCUnsupportedMediaTypeErrorResponse,
      SCValidationErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCMultiSearchRequest';
    this.responseBodyName = 'SCMultiSearchResponse';
    this.statusCodeSuccess = StatusCodes.OK;
    this.urlPath = '/search/multi';
  }
}
