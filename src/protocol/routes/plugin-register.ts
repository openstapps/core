/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {StatusCodes} from 'http-status-codes';
import {JSONSchema7} from 'json-schema';
import {SCInternalServerErrorResponse} from '../errors/internal-server-error';
import {SCMethodNotAllowedErrorResponse} from '../errors/method-not-allowed';
import {SCNotFoundErrorResponse} from '../errors/not-found';
import {SCParametersNotAcceptable} from '../errors/parameters-not-acceptable';
import {SCPluginAlreadyRegisteredErrorResponse} from '../errors/plugin-already-registered';
import {SCPluginRegisteringFailedErrorResponse} from '../errors/plugin-registering-failed';
import {SCRequestBodyTooLargeErrorResponse} from '../errors/request-body-too-large';
import {SCSyntaxErrorResponse} from '../errors/syntax-error';
import {SCAbstractRoute, SCRouteHttpVerbs} from '../route';

/**
 * Plugin register request
 *
 * @validatable
 */
export type SCPluginRegisterRequest = SCPluginAdd | SCPluginRemove;

/**
 * Plugin request for adding a plugin registration to the backend
 */
export interface SCPluginAdd {
  /**
   * The desired action, so whether the plugin should be added or removed
   */
  action: 'add';

  /**
   * Plugin information needed for its registration
   */
  plugin: SCPluginMetaData;
}

/**
 * Plugin request for removing a plugin registration from the backend
 */
export interface SCPluginRemove {
  /**
   * The desired action, so whether the plugin should be added or removed
   */
  action: 'remove';

  /**
   * The route of the plugin you want to remove
   */
  route: string;
}

/**
 * Plugin meta data - contains needed information for a plugin registration
 */
export interface SCPluginMetaData {
  /**
   * The address of the plugin, to which the backend routes the requests
   */
  address: string;

  /**
   * The name of the plugin (for debugging purposes, to more easily identify conflicts)
   */
  name: string;

  /**
   * How the requests of the plugin looks like, a JSON schema for validation
   */
  requestSchema: JSONSchema7;

  /**
   * How the responses of the plugin looks like, a JSON schema for validation
   */
  responseSchema: JSONSchema7;

  /**
   * The desired route, for example /feedback.
   */
  route: string;
}

/**
 * Plugin register response
 *
 * @validatable
 */
export interface SCPluginRegisterResponse {
  /**
   * Whether the desired action succeeded or failed (true for success, false if an error occurred)
   */
  success: boolean;
}

/**
 * Route to register plugins
 */
export class SCPluginRegisterRoute extends SCAbstractRoute {
  constructor() {
    super();
    this.errorNames = [
      SCInternalServerErrorResponse,
      SCMethodNotAllowedErrorResponse,
      SCNotFoundErrorResponse,
      SCParametersNotAcceptable,
      SCPluginAlreadyRegisteredErrorResponse,
      SCPluginRegisteringFailedErrorResponse,
      SCRequestBodyTooLargeErrorResponse,
      SCSyntaxErrorResponse,
    ];
    this.method = SCRouteHttpVerbs.POST;
    this.requestBodyName = 'SCPluginRegisterRequest';
    this.responseBodyName = 'SCPluginRegisterResponse';
    this.statusCodeSuccess = StatusCodes.OK;
    this.urlPath = '/plugin/register';
  }
}
