/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Capsulation for a map with a string as key with values of type `T`
 *
 * !!! BEWARE !!!
 * Can't be refactored to a `Map<K, V>`, because it can't be serialized via JSON.stringify(map)
 *
 * @typeparam T Can be any type.
 */
export interface SCMap<T> {
  /**
   * One value for each key
   */
  [key: string]: T;
}

/**
 * Restricted map with keys, limited to values of `U`, and corresponding values of type `T`
 *
 * !!! BEWARE !!!
 * Can't be refactored to a `Map<K, V>`, because it can't be serialized via JSON.stringify(map)
 * Also note, that this is a type not an interface
 *
 * @typeparam U Must be a type the `in` operator can be applied to and contains only strings or numbers
 * @typeparam T Can be any type
 */
export type SCRestrictedMap<U extends string | number, T> = {
  /**
   * One value for each key
   */
  [key in U]: T;
};
