/*
 * Copyright (C) 2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {SCMap} from '../general/map';
import {SCAuthorizationProviderType} from './authorization';

export interface SCFeatureConfiguration {
  /**
   * Map of extern services mapped by their name (statically)
   */
  extern?: SCMap<SCFeatureConfigurationExtern>;

  /**
   * Map of plugins registered with the backend mapped by their name.
   */
  plugins?: SCMap<SCFeatureConfigurationPlugin>;
}

export interface SCFeatureConfigurationPlugin {
  /**
   * URL path registered with the backend
   */
  urlPath: string;
}

export interface SCFeatureConfigurationExtern {
  /**
   * Key of authorization provider available in SCConfigFile
   */
  authProvider?: SCAuthorizationProviderType;

  /**
   * URL of extern service
   */
  url: string;
}
