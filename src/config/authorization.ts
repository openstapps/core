/*
 * Copyright (C) 2021-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {SCUserConfigurationMap} from './user';

/**
 * Supported authorization provider types
 *
 * @see https://datatracker.ietf.org/doc/html/rfc6749#section-1.3.1
 * @see https://github.com/gbv/paia
 */
export type SCAuthorizationProviderType = 'default' | 'paia';

/**
 * An authorization provider complete configuration
 */
export interface SCAuthorizationProvider {
  /**
   * An authorization provider client configuration
   */
  client: SCAuthorizationProviderClient;

  /**
   * An authorization provider endpoints configuration
   */
  endpoints: SCAuthorizationProviderEndpoints;
}

/**
 * An authorization provider client configuration
 */
export interface SCAuthorizationProviderClient {
  /**
   * Client ID
   */
  clientId: string;

  /**
   * Scopes to request
   */
  scopes: string;

  /**
   * Main url to reach authorization provider
   */
  url: string;
}

/**
 * An authorization provider endpoints configuration
 */
export interface SCAuthorizationProviderEndpoints {
  /**
   * URL to start authentication flow
   */
  authorization: string;

  /**
   * URL to end current session
   */
  endSession?: string;

  /**
   * Mapping of how to create SCUser from userinfo endpoint response (using JSONPath syntax)
   *
   * @see https://www.npmjs.com/package/jsonpath
   */
  mapping: SCUserConfigurationMap;

  /**
   * URL to revoke a token
   */
  revoke?: string;

  /**
   * URL to get access Token
   */
  token: string;

  /**
   * URL to general user info endpoint
   */
  userinfo: string;
}
