/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * An abstract action that can be executed by a failed watcher
 *
 * This is extended by actual actions.
 */
export interface SCMonitoringAction {
  /**
   * Message that is used by the action
   *
   * Mustache syntax is supported to evaluate the query.
   */
  message: string;

  /**
   * The type of the action
   */
  type: 'log' | 'mail';
}

/**
 * A log action which can be executed from a failed watcher
 */
export interface SCMonitoringLogAction extends SCMonitoringAction {
  /**
   * A prefix for the logged messages
   */
  prefix: string;

  /**
   * The type of the action
   */
  type: 'log';
}

/**
 * A mail task which can be executed from a failed watcher
 */
export interface SCMonitoringMailAction extends SCMonitoringAction {
  /**
   * A list of addresses to send the mails to
   */
  recipients: string[];

  /**
   * A subject line for all mails
   */
  subject: string;

  /**
   * The type of the action
   */
  type: 'mail';
}

/**
 * A list of events to describe the execution interval of monitoring triggers
 */
export type SCMonitoringExecutionCalendarEvents = 'hourly' | 'daily' | 'weekly' | 'monthly';

/**
 * A trigger which determines the execution time of a watcher
 */
export interface SCMonitoringTrigger {
  /**
   * A cron like syntax to describe when this trigger should be executed
   */
  executionTime: SCMonitoringExecutionCalendarEvents | string;

  /**
   * A name of the trigger that explains when it executes
   */
  name: string;
}

/**
 * A condition which is met, when the result length is higher than the minimum length
 */
export interface SCMonitoringMinimumLengthCondition {
  /**
   * The minimum length
   */
  length: number;

  /**
   * Type of the condition
   */
  type: 'MinimumLength';
}

/**
 * A condition which is met, when the result length is smaller than the maximum length
 */
export interface SCMonitoringMaximumLengthCondition {
  /**
   * The maximum length
   */
  length: number;

  /**
   * Type of the condition
   */
  type: 'MaximumLength';
}

/**
 * A watcher that evaluates a query, when triggered
 */
export interface SCMonitoringWatcher {
  /**
   * A list of actions that are executed, when the watcher fails
   */
  actions: Array<SCMonitoringLogAction | SCMonitoringMailAction>;

  /**
   * A list of conditions that have to match the result of the query
   *
   * If not all conditions are met, the watcher will fail and all actions are executed
   */
  conditions: Array<SCMonitoringMaximumLengthCondition | SCMonitoringMinimumLengthCondition>;

  /**
   * A name for the watcher
   */
  name: string;

  /**
   * Query to execute against the database
   */
  query: unknown;

  /**
   * A list of triggers
   */
  triggers: SCMonitoringTrigger[];
}

/**
 * A complete monitoring configuration for the backend
 */
export interface SCMonitoringConfiguration {
  /**
   * A list of actions that are executed, when a watcher fails
   */
  actions: Array<SCMonitoringMailAction | SCMonitoringLogAction>;

  /**
   * A list of watches
   */
  watchers: SCMonitoringWatcher[];
}
