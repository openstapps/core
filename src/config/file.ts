/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCLicensePlate} from '../general/namespaces';
import {SCAppConfiguration} from './app';
import {SCAuthorizationProvider, SCAuthorizationProviderType} from './authorization';
import {SCBackendConfiguration, SCBackendInternalConfiguration} from './backend';

/**
 * A configuration file that configures app and backend
 *
 * @validatable
 */
export interface SCConfigFile {
  /**
   * Configuration for the app that is visible to clients
   */
  app: SCAppConfiguration;

  /**
   * Configuration for the supported authorization providers
   */
  auth: {[key in SCAuthorizationProviderType]?: SCAuthorizationProvider};

  /**
   * Configuration for the backend that is visible to clients
   */
  backend: SCBackendConfiguration;

  /**
   * Configuration that is not visible to clients
   */
  internal: SCBackendInternalConfiguration;

  /**
   * UID of the university, e.g. the license plate
   */
  uid: SCLicensePlate;
}
