/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Polygon} from 'geojson';
import {SCTranslations} from '../general/i18n';
import {SCMap} from '../general/map';
import {SCLanguageSetting, SCSetting, SCUserGroupSetting} from '../things/setting';
import {SCAuthorizationProviderType} from './authorization';
import {SCFeatureConfiguration} from './feature';

/**
 * An app configuration menu item
 */
export interface SCAppConfigurationMenuItem {
  /**
   * Key of authorization provider available in SCConfigFile
   * Restricting and enabling the usage of this item
   */
  authProvider?: SCAuthorizationProviderType;

  /**
   * Icon for the menu item
   */
  icon: string;

  /**
   * Route inside the app
   */
  route: string;

  /**
   * Title of the route
   */
  title: string;

  /**
   * Translations for the menu item
   */
  translations: SCTranslations<SCAppConfigurationMenuItemTranslationTitle>;
}

/**
 * An app configuration menu category
 */
export interface SCAppConfigurationMenuCategory {
  /**
   * Icon for the menu category
   */
  icon: string;

  /**
   * A list of items that belong to the category
   */
  items: SCAppConfigurationMenuItem[];

  /**
   * Title of the category
   */
  title: string;

  /**
   * Route inside the app
   */
  route: string;

  /**
   * Translations for the menu category
   */
  translations: SCTranslations<SCAppConfigurationMenuCategoryTranslationTitle>;
}

/**
 * An app configuration
 */
export interface SCAppConfiguration {
  /**
   * The about page
   *
   * Mapping route -> page config
   */
  aboutPages: SCMap<SCAboutPage>;

  /**
   * Polygon that encapsulates the main campus
   */
  campusPolygon: Polygon;

  /**
   * Maps of enabled features (plugins and external services)
   */
  features: SCFeatureConfiguration;

  /**
   * A URL where images are available
   */
  imageUrl?: string;

  /**
   * A list of available menu categories in the app
   */
  menus: SCAppConfigurationMenuCategory[];

  /**
   * Name for the app
   */
  name: string;

  /**
   * URL to a file containing the privacy policy
   */
  privacyPolicyUrl: string;

  /**
   * A list of available settings in the app
   * !Important! Use provided specific settings, for other settings use general SCSetting
   */
  settings: Array<SCUserGroupSetting | SCLanguageSetting | SCSetting>;

  /**
   * Map of store URLs
   */
  storeUrl?: SCAppConfigurationStoreUrl;

  /**
   * URL where a web instance of the app is available
   */
  url?: string;
}

/**
 * URLs of published apps
 */
export interface SCAppConfigurationStoreUrl {
  /**
   * Google Play Store URL
   */
  android?: string;
  /**
   * Apple App Store URL
   */
  ios?: string;
  /**
   * Microsoft Store URL
   */
  uwp?: string;
}

/**
 * Translatable property of a menu item
 */
export interface SCAppConfigurationMenuItemTranslationTitle {
  /**
   * Translation of the title of a menu item
   */
  title: string;
}

/**
 * Translatable property of a menu category
 */
export interface SCAppConfigurationMenuCategoryTranslationTitle {
  /**
   * Translation of the name of a menu category
   */
  title: string;
}

export enum SCAboutPageContentType {
  SECTION = 'section',
  ROUTER_LINK = 'router link',
  TABLE = 'table',
  MARKDOWN = 'markdown',
}

export interface SCAboutPageTranslationTitle {
  /**
   * Translation of the title
   */
  title: string;
}

export interface SCAboutPageTranslationValue {
  /**
   * Translation of the value
   */
  value: string;
}

/**
 * A (mostly) self-contained section, akin to markdown `# Title`
 */
export interface SCAboutPageSection {
  /**
   * If the section should be contained in a card
   */
  card?: true;

  /**
   * The content of the section
   */
  content: SCAboutPageContent;

  /**
   * The title of the section
   */
  title: string;

  /**
   * Translations
   */
  translations: SCTranslations<SCAboutPageTranslationTitle>;

  /**
   * Type
   */
  type: SCAboutPageContentType.SECTION;
}

/**
 * A router link that can lead to a new page
 *
 * For external links, prefer markdown `[destination](link)`
 */
export interface SCAboutPageRouterLink {
  /**
   * Icon of the destination
   */
  icon?: string;

  /**
   * Router link
   */
  link: string;

  /**
   * Title of the destination
   */
  title: string;

  /**
   * Translations
   */
  translations: SCTranslations<SCAboutPageTranslationTitle>;

  /**
   * Type
   */
  type: SCAboutPageContentType.ROUTER_LINK;
}

/**
 * A simple table element
 */
export interface SCAboutPageTable {
  /**
   * Rows of the table
   */
  rows: SCAboutPageContent[][];

  /**
   * Type
   */
  type: SCAboutPageContentType.TABLE;
}

/**
 * A markdown element
 */
export interface SCAboutPageMarkdown {
  /**
   * Translations
   */
  translations: SCTranslations<SCAboutPageTranslationValue>;

  /**
   * Type
   */
  type: SCAboutPageContentType.MARKDOWN;

  /**
   * Value (Markdown)
   */
  value: string;
}

export type SCAboutPageContent =
  | SCAboutPageMarkdown
  | SCAboutPageTable
  | SCAboutPageSection
  | SCAboutPageRouterLink;

/**
 * Root of the about page
 */
export interface SCAboutPage {
  /**
   * Content of the page
   */
  content: SCAboutPageContent[];

  /**
   * Header (title) of the page
   */
  title: string;

  /**
   * Translations
   */
  translations: SCTranslations<SCAboutPageTranslationTitle>;
}
