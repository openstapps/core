/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {SCEvent, SCEventMeta, SCEventWithoutReferences} from './abstract/event';
import {SCThingMeta, SCThingType} from './abstract/thing';
import {
  SCThingWithCategories,
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from './abstract/thing-with-categories';

/**
 * An academic event without references
 */
export interface SCAcademicEventWithoutReferences
  extends SCEventWithoutReferences,
    SCThingWithCategoriesWithoutReferences<SCAcademicEventCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * Majors of the academic event that this event belongs to
   *
   * @aggregatable
   * @filterable
   * @keyword
   */
  majors?: string[];

  /**
   * Original unmapped category from the source of the academic event
   *
   * @filterable
   * @keyword
   */
  originalCategory?: string;

  /**
   * Translated fields of an academic event
   */
  translations?: SCTranslations<SCAcademicEventTranslatableProperties>;

  /**
   * Type of an academic event
   */
  type: SCThingType.AcademicEvent;
}

/**
 * An academic event
 *
 * @validatable
 * @indexable
 */
export interface SCAcademicEvent
  extends SCEvent,
    SCAcademicEventWithoutReferences,
    SCThingWithCategories<SCAcademicEventCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * Translated fields of an academic event
   */
  translations?: SCTranslations<SCAcademicEventTranslatableProperties>;

  /**
   * Type of an academic event
   */
  type: SCThingType.AcademicEvent;
}

/**
 * Categories of academic events
 */
export type SCAcademicEventCategories =
  | 'lecture'
  | 'seminar'
  | 'integrated course'
  | 'written exam'
  | 'oral exam'
  | 'tutorial'
  | 'project'
  | 'colloquium'
  | 'practicum'
  | 'introductory class'
  | 'course'
  | 'practicum introduction'
  | 'excursion'
  | 'exercise'
  | 'special';

/**
 * Translatable properties of an academic event
 */
export interface SCAcademicEventTranslatableProperties extends SCThingWithCategoriesTranslatableProperties {
  /**
   * Translations of the majors of the academic event that this event belongs to
   *
   * @keyword
   */
  majors?: string[];

  /**
   * Translation of the original unmapped category from the source of the academic event
   *
   * @keyword
   */
  originalCategory?: string;
}

/**
 * Meta information about academic events
 */
export class SCAcademicEventMeta extends SCThingMeta implements SCMetaTranslations<SCAcademicEvent> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCEventMeta().fieldTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCAcademicEventCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.de,
      majors: 'Hauptfächer',
      originalCategory: 'ursprüngliche Kategorie',
    },
    en: {
      ...new SCEventMeta().fieldTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCAcademicEventCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.en,
      majors: 'majors',
      originalCategory: 'original category',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCEventMeta().fieldValueTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCAcademicEventCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.de,
      categories: {
        'colloquium': 'Kolloquium',
        'course': 'Kurs',
        'excursion': 'Exkursion',
        'exercise': 'Übung',
        'integrated course': 'Integrierter Kurs',
        'introductory class': 'Einführungsveranstaltung',
        'lecture': 'Vorlesung',
        'oral exam': 'mündliche Prüfung',
        'practicum': 'Praktikum',
        'practicum introduction': 'Einführungspraktikum',
        'project': 'Projekt',
        'seminar': 'Seminar',
        'special': 'Sonderveranstalltung',
        'tutorial': 'Tutorium',
        'written exam': 'Schriftilche Prüfung',
      },
      type: 'akademische Veranstaltung',
    },
    en: {
      ...new SCEventMeta().fieldValueTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCAcademicEventCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.en,
      type: SCThingType.AcademicEvent,
    },
  };
}
