/*
 * Copyright (C) 2021-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {
  SCCreativeWork,
  SCCreativeWorkMeta,
  SCCreativeWorkTranslatableProperties,
  SCCreativeWorkWithoutReferences,
} from './abstract/creative-work';
import {SCThingMeta, SCThingType} from './abstract/thing';
import {
  SCAcademicPriceGroup,
  SCThingThatCanBeOffered,
  SCThingThatCanBeOfferedMeta,
  SCThingThatCanBeOfferedTranslatableProperties,
  SCThingThatCanBeOfferedWithoutReferences,
} from './abstract/thing-that-can-be-offered';
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from './abstract/thing-with-categories';

/**
 * Categories of a periodical
 */
export type SCPeriodicalCategories = 'journal' | 'electronic';

/**
 * A periodical without references
 */
export interface SCPeriodicalWithoutReferences
  extends SCCreativeWorkWithoutReferences,
    SCThingThatCanBeOfferedWithoutReferences,
    SCThingWithCategoriesWithoutReferences<SCPeriodicalCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * Categories of a periodical
   */
  categories: SCPeriodicalCategories[];
  /**
   * A list of ISSNs of a periodical
   *
   * @filterable
   * @keyword
   */
  ISSNs?: string[];

  /**
   * Translated properties of a periodical
   */
  translations?: SCTranslations<SCPeriodicalTranslatableFields>;

  /**
   * Type of a periodical
   */
  type: SCThingType.Periodical;
}

/**
 * A publication published at regular intervals (e.g. a magazine or newspaper)
 *
 * @validatable
 * @indexable
 */
export interface SCPeriodical
  extends SCCreativeWork,
    SCThingThatCanBeOffered<SCAcademicPriceGroup>,
    SCPeriodicalWithoutReferences {
  /**
   * Translated properties of a periodical
   */
  translations?: SCTranslations<SCPeriodicalTranslatableFields>;

  /**
   * Type of a periodical
   */
  type: SCThingType.Periodical;
}

/**
 * Translatable properties of a periodical
 */
export interface SCPeriodicalTranslatableFields
  extends SCThingWithCategoriesTranslatableProperties,
    SCThingThatCanBeOfferedTranslatableProperties,
    SCCreativeWorkTranslatableProperties {}

/**
 * Meta information about a periodical
 */
export class SCPeriodicalMeta extends SCThingMeta implements SCMetaTranslations<SCPeriodical> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCCreativeWorkMeta().fieldTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCPeriodicalCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.de,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldTranslations.de,
      categories: 'Format',
      ISSNs: 'ISSN',
    },
    en: {
      ...new SCCreativeWorkMeta().fieldTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCPeriodicalCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.en,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldTranslations.en,
      categories: 'format',
      ISSNs: 'ISSN',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCCreativeWorkMeta().fieldValueTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCPeriodicalCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.de,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldValueTranslations.de,
      type: 'Periodikum',
      categories: {
        electronic: 'E-Journal',
        journal: 'Zeitschrift',
      },
    },
    en: {
      ...new SCCreativeWorkMeta().fieldValueTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCPeriodicalCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.en,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldValueTranslations.en,
      type: 'periodical',
      categories: {
        electronic: 'E-Journal',
        journal: 'journal',
      },
    },
  };
}
