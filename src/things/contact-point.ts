/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {SCMetaTranslations} from '../general/i18n';
import {SCThing, SCThingMeta, SCThingType, SCThingWithoutReferences} from './abstract/thing';
import {SCRoomWithoutReferences} from './room';

/**
 * A contact point without references
 */
export interface SCContactPointWithoutReferences extends SCThingWithoutReferences {
  /**
   * E-mail at the work location
   *
   * @keyword
   */
  email?: string;

  /**
   * Fax number at the work location
   *
   * @keyword
   */
  faxNumber?: string;

  /**
   * Office hours for contacting someone at the work location
   *
   * @see http://wiki.openstreetmap.org/wiki/Key:opening_hours/specification
   * @keyword
   */
  officeHours?: string;

  /**
   * Contact number at the work location
   *
   * @keyword
   */
  telephone?: string;

  /**
   * Type of a contact point
   */
  type: SCThingType.ContactPoint;

  /**
   * URL at the work location
   *
   * @keyword
   */
  url?: string;
}

/**
 * A contact point
 *
 * @see http://schema.org/ContactPoint
 * @validatable
 * @indexable
 */
export interface SCContactPoint extends SCContactPointWithoutReferences, SCThing {
  /**
   * Exact place where work is performed
   */
  areaServed?: SCRoomWithoutReferences;

  /**
   * Type of a contact point
   */
  type: SCThingType.ContactPoint;
}

/**
 * Meta information about a contact point
 */
export class SCContactPointMeta extends SCThingMeta implements SCMetaTranslations<SCContactPoint> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      areaServed: 'Arbeitsraum',
      email: 'E-Mail-Addresse',
      faxNumber: 'Faxnummer',
      officeHours: 'Sprechzeiten',
      telephone: 'Telefonnummer',
      url: 'Link',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      areaServed: 'location',
      email: 'email address',
      faxNumber: 'fax number',
      officeHours: 'office hours',
      telephone: 'telephone number',
      url: 'link',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
      type: 'Kontaktinformation',
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
      type: SCThingType.ContactPoint,
    },
  };
}
