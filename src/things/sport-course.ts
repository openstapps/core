/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations} from '../general/i18n';
import {SCEvent, SCEventMeta, SCEventWithoutReferences} from './abstract/event';
import {SCThingMeta, SCThingType} from './abstract/thing';

/**
 * A sport course without references
 */
export interface SCSportCourseWithoutReferences extends SCEventWithoutReferences {
  /**
   * Type of a sport course
   */
  type: SCThingType.SportCourse;
}

/**
 * A sport course
 *
 * @validatable
 * @indexable
 */
export interface SCSportCourse extends SCEvent, SCSportCourseWithoutReferences {
  /**
   * Type of a sport course
   */
  type: SCThingType.SportCourse;
}

/**
 * Meta information about a sport course
 */
export class SCSportCourseMeta extends SCThingMeta implements SCMetaTranslations<SCSportCourse> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCEventMeta().fieldTranslations.de,
    },
    en: {
      ...new SCEventMeta().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCEventMeta().fieldValueTranslations.de,
      type: 'Sportkurs',
    },
    en: {
      ...new SCEventMeta().fieldValueTranslations.en,
      type: SCThingType.SportCourse,
    },
  };
}
