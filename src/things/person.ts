/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCNationality} from '../general/i18n';
import {SCISO8601Date} from '../general/time';
import {SCThing, SCThingMeta, SCThingType, SCThingWithoutReferences} from './abstract/thing';
import {SCBuildingWithoutReferences} from './building';
import {SCContactPointWithoutReferences} from './contact-point';
import {SCOrganizationWithoutReferences} from './organization';
import {SCPointOfInterestWithoutReferences} from './point-of-interest';
import {SCRoomWithoutReferences} from './room';

/**
 * A person without references
 */
export interface SCPersonWithoutReferences extends SCThingWithoutReferences {
  /**
   * Additional first names of the person.
   *
   * @filterable
   * @keyword
   */
  additionalName?: string;

  /**
   * The birth date of the person.
   *
   * @filterable
   */
  birthDate?: SCISO8601Date;

  /**
   * The private email address of the person.
   *
   * @TJS-format email
   * @filterable
   * @keyword
   */
  email?: string;

  /**
   * The family name of the person.
   *
   * @filterable
   * @keyword
   */
  familyName?: string;

  /**
   * The private fax number of the person.
   *
   * @filterable
   * @keyword
   */
  faxNumber?: string;

  /**
   * The gender of the person.
   *
   * @filterable
   */
  gender?: SCPersonGender;

  /**
   * The first name of the person.
   *
   * @filterable
   * @keyword
   */
  givenName?: string;

  /**
   * Honorific prefix of the person.
   *
   * @filterable
   * @keyword
   */
  honorificPrefix?: string;

  /**
   * Honorific suffix of the person.
   *
   * @filterable
   * @keyword
   */
  honorificSuffix?: string;

  /**
   * Titles of jobs that the person has.
   *
   * @filterable
   * @keyword
   */
  jobTitles?: string[];

  /**
   * The complete name of the person combining all the parts of the name into one.
   *
   * @filterable
   * @text
   */
  name: string;

  /**
   * The nationality of the person.
   */
  nationality?: SCNationality;

  /**
   * The private telephone number of the person.
   *
   * @keyword
   */
  telephone?: string;

  /**
   * Type of a person
   */
  type: SCThingType.Person;
}

/**
 * A person
 *
 * @validatable
 * @indexable
 */
export interface SCPerson extends SCPersonWithoutReferences, SCThing {
  /**
   * Organization the person works for
   */
  affiliations?: SCOrganizationWithoutReferences[];

  /**
   * A list of homes of the person
   */
  homeLocations?: Array<
    SCBuildingWithoutReferences | SCPointOfInterestWithoutReferences | SCRoomWithoutReferences
  >;

  /**
   * Type of a person
   */
  type: SCThingType.Person;

  /**
   * Locations where the person performs her/his work
   */
  workLocations?: SCContactPointWithoutReferences[];
}

/**
 * Meta information about a person
 */
export class SCPersonMeta extends SCThingMeta implements SCMetaTranslations<SCPerson> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      additionalName: 'Zusatzname',
      affiliations: 'Zugehörigkeiten',
      birthDate: 'Geburtsdatum',
      email: 'E-Mail',
      familyName: 'Nachname',
      faxNumber: 'Faxnummer',
      gender: 'Geschlecht',
      givenName: 'Vorname',
      homeLocations: 'Heimatstandorte',
      honorificPrefix: 'Ehrentitel',
      honorificSuffix: 'Ehrentitel (Suffix)',
      jobTitles: 'Berufsbezeichnungen',
      nationality: 'Staatsangehörigkeit',
      telephone: 'Telefonnummer',
      workLocations: 'Arbeitsstandorte',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      additionalName: 'additional name',
      affiliations: 'affiliations',
      birthDate: 'birth date',
      contactDetails: 'contact details',
      email: 'email',
      familyName: 'family name',
      faxNumber: 'fax',
      gender: 'gender',
      givenName: 'given name',
      homeLocations: 'home locations',
      honorificPrefix: 'honorific title',
      honorificSuffix: 'honorific title (suffix)',
      jobTitles: 'job titles',
      nationality: 'nationality',
      telephone: 'telephone',
      workLocations: 'work locations',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
      gender: {
        female: 'weiblich',
        inter: 'divers',
        male: 'männlich',
        undefined: 'undefiniert',
      },
      type: 'Person',
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
      type: SCThingType.Person,
    },
  };
}

/**
 * Gender of a person
 */
export type SCPersonGender = 'male' | 'female' | 'inter' | 'undefined';
