/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Feature, FeatureCollection, GeometryObject, LineString} from 'geojson';
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {
  SCThingMeta,
  SCThingTranslatableProperties,
  SCThingType,
  SCThingWithoutReferences,
} from './abstract/thing';
import {SCThingInPlace, SCThingInPlaceMeta} from './abstract/thing-in-place';
import {SCPointOfInterestWithoutReferences} from './point-of-interest';
import {SCRoomWithoutReferences} from './room';

/**
 * A floor without references
 */
export interface SCFloorWithoutReferences extends SCThingWithoutReferences {
  /**
   * Floor name in the place it is in e.g. "first floor", "ground floor". This doesn't reference the building name.
   *
   * @text
   */
  floorName: string;

  /**
   * Floor plan
   */
  plan: SCFloorFeatureCollectionWithPlaces<LineString>;

  /**
   * Translated fields of a floor
   */
  translations?: SCTranslations<SCFloorTranslatableProperties>;

  /**
   * Type of a floor
   */
  type: SCThingType.Floor;
}

/**
 * A floor
 *
 * @validatable
 * @indexable
 */
export interface SCFloor extends SCFloorWithoutReferences, SCThingInPlace {
  /**
   * Translated fields of a floor
   */
  translations?: SCTranslations<SCFloorTranslatableProperties>;

  /**
   * Type of a floor
   */
  type: SCThingType.Floor;
}

/**
 * A feature collection
 */
export interface SCFloorFeatureCollectionWithPlaces<T extends GeometryObject> extends FeatureCollection<T> {
  /**
   * Features of the collection
   */
  features: Array<SCFloorFeatureWithPlace<T>>;
}

/***
 * A feature with a place
 */
export interface SCFloorFeatureWithPlace<T extends GeometryObject>
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  extends Feature<T, any> {
  /**
   * The place of the feature
   */
  place?: SCRoomWithoutReferences | SCPointOfInterestWithoutReferences;
}

/**
 * Translatable properties of a floor
 */
export interface SCFloorTranslatableProperties extends SCThingTranslatableProperties {
  /**
   * Translation of the floor name
   *
   * @text
   */
  floorName?: string;
}

/**
 * Meta information about floors
 */
export class SCFloorMeta extends SCThingMeta implements SCMetaTranslations<SCFloor> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingInPlaceMeta().fieldTranslations.de,
      floorName: 'Etagenbezeichnung',
      plan: 'Grundriss',
    },
    en: {
      ...new SCThingInPlaceMeta().fieldTranslations.en,
      floorName: 'floor name',
      plan: 'plan',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingInPlaceMeta().fieldValueTranslations.de,
      type: 'Etage',
    },
    en: {
      ...new SCThingInPlaceMeta().fieldValueTranslations.en,
      type: SCThingType.Floor,
    },
  };
}
