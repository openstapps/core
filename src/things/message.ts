/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {SCISO8601Date} from '../general/time';
import {
  SCCreativeWork,
  SCCreativeWorkMeta,
  SCCreativeWorkTranslatableProperties,
  SCCreativeWorkWithoutReferences,
} from './abstract/creative-work';
import {SCThingMeta, SCThingType} from './abstract/thing';
import {SCThingThatCanBeOfferedTranslatableProperties} from './abstract/thing-that-can-be-offered';
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from './abstract/thing-with-categories';
import {SCUserGroup} from './abstract/user-groups';
import {SCOrganizationWithoutReferences} from './organization';

/**
 * Categories of a message
 */
export type SCMessageCategories = 'news';

/**
 * A message without references
 */
export interface SCMessageWithoutReferences
  extends SCCreativeWorkWithoutReferences,
    SCThingWithCategoriesWithoutReferences<SCMessageCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * Organizational unit for which the message is intended
   */
  audienceOrganizations?: SCOrganizationWithoutReferences[];

  /**
   * Roles for which the message is intended
   *
   * @filterable
   */
  audiences: SCUserGroup[];

  /**
   * Categories of a message
   */
  categories: SCMessageCategories[];

  /**
   * When the message was created
   *
   * @filterable
   */
  dateCreated?: SCISO8601Date;

  /**
   * Message itself
   *
   * @text
   */
  messageBody: string;

  /**
   * An index for applying a custom sorting of multiple messages
   */
  sequenceIndex?: number;

  /**
   * Translated fields of a message
   */
  translations?: SCTranslations<SCMessageTranslatableProperties>;

  /**
   * Type of a message
   */
  type: SCThingType.Message;
}

/**
 * A message
 *
 * @validatable
 * @indexable
 */
export interface SCMessage extends SCCreativeWork, SCMessageWithoutReferences {
  /**
   * Translated fields of a message
   */
  translations?: SCTranslations<SCMessageTranslatableProperties>;

  /**
   * Type of a message
   */
  type: SCThingType.Message;
}

/**
 * Translatable properties of a message
 */
export interface SCMessageTranslatableProperties
  extends SCCreativeWorkTranslatableProperties,
    SCThingThatCanBeOfferedTranslatableProperties {
  /**
   * Message itself
   *
   * @text
   */
  messageBody?: string;
}

/**
 * Meta information about messages
 */
export class SCMessageMeta extends SCThingMeta implements SCMetaTranslations<SCMessage> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCCreativeWorkMeta().fieldTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCMessageCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.de,
      audienceOrganizations: 'Zielgruppenorganisationen',
      audiences: 'Zielgruppen',
      dateCreated: 'Erstellungsdatum',
      messageBody: 'Nachrichteninhalt',
      sequenceIndex: 'Sequenzindex',
    },
    en: {
      ...new SCCreativeWorkMeta().fieldTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCMessageCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.en,
      audienceOrganizations: 'audience organizations',
      audiences: 'audiences',
      dateCreated: 'date created',
      messageBody: 'message body',
      sequenceIndex: 'sequence index',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCCreativeWorkMeta().fieldValueTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCMessageCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.de,
      audiences: {
        employees: 'Angestellte',
        guests: 'Gäste',
        students: 'Studenten',
      },
      categories: {
        news: 'Neuigkeiten',
      },
      type: 'Nachricht',
    },
    en: {
      ...new SCCreativeWorkMeta().fieldValueTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCMessageCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.en,
      type: SCThingType.Message,
    },
  };
}
