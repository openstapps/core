/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {SCMap} from '../general/map';
import {SCPlace, SCPlaceWithoutReferences, SCPlaceWithoutReferencesMeta} from './abstract/place';

import {SCThingMeta, SCThingType} from './abstract/thing';
import {SCThingInPlace, SCThingInPlaceMeta} from './abstract/thing-in-place';
import {
  SCThingThatAcceptsPayments,
  SCThingThatAcceptsPaymentsWithoutReferences,
  SCThingThatAcceptsPaymentsWithoutReferencesMeta,
} from './abstract/thing-that-accepts-payments';
import {
  SCThingWithCategories,
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from './abstract/thing-with-categories';

/**
 * Categories of a room
 */
export type SCRoomCategories =
  | 'cafe'
  | 'canteen'
  | 'computer'
  | 'education'
  | 'laboratory'
  | 'learn'
  | 'library'
  | 'lounge'
  | 'office'
  | 'restaurant'
  | 'restroom'
  | 'student canteen'
  | 'student union';

/**
 * A room without references
 */
export interface SCRoomWithoutReferences
  extends SCPlaceWithoutReferences,
    SCThingThatAcceptsPaymentsWithoutReferences,
    SCThingWithCategoriesWithoutReferences<SCRoomCategories, SCRoomSpecificValues> {
  /**
   * The name of the floor in which the room is in.
   *
   * @filterable
   * @text
   */
  floorName?: string;

  /**
   * The inventory of the place/room as a list of items and their quantity.
   */
  inventory?: SCMap<number>;

  /**
   * Translations of specific values of the object
   *
   * Take precedence over "main" value for selected languages.
   */
  translations?: SCTranslations<SCThingWithCategoriesTranslatableProperties>;

  /**
   * Type of the room
   */
  type: SCThingType.Room;
}

/**
 * A room
 *
 * @validatable
 * @indexable
 */
export interface SCRoom
  extends SCRoomWithoutReferences,
    SCThingInPlace,
    SCThingThatAcceptsPayments,
    SCPlace,
    SCThingWithCategories<SCRoomCategories, SCRoomSpecificValues> {
  /**
   * Translations of specific values of the object
   *
   * Take precedence over "main" value for selected languages.
   */
  translations?: SCTranslations<SCThingWithCategoriesTranslatableProperties>;

  /**
   * Type of the room
   */
  type: SCThingType.Room;
}

/**
 * Category specific values of a room
 */
export interface SCRoomSpecificValues extends SCThingWithCategoriesSpecificValues {
  /**
   * Category specific opening hours of the room
   *
   * @keyword
   */
  openingHours?: string;

  /**
   * Category specific service hours of the room (e.g. cooked food serving hours)
   *
   * @see http://wiki.openstreetmap.org/wiki/Key:opening_hours/specification
   * @keyword
   */
  serviceHours?: string;
}

/**
 * Meta information about a place
 */
export class SCRoomMeta extends SCThingMeta implements SCMetaTranslations<SCRoom> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCPlaceWithoutReferencesMeta().fieldTranslations.de,
      ...new SCThingThatAcceptsPaymentsWithoutReferencesMeta().fieldTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<SCRoomCategories, SCRoomSpecificValues>()
        .fieldTranslations.de,
      ...new SCThingInPlaceMeta().fieldTranslations.de,
      floorName: 'Etagenbezeichnung',
      inventory: 'Bestand',
      serviceHours: 'Servicezeiten',
    },
    en: {
      ...new SCPlaceWithoutReferencesMeta().fieldTranslations.en,
      ...new SCThingThatAcceptsPaymentsWithoutReferencesMeta().fieldTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<SCRoomCategories, SCRoomSpecificValues>()
        .fieldTranslations.en,
      ...new SCThingInPlaceMeta().fieldTranslations.en,
      floorName: 'floor name',
      inventory: 'inventory',
      serviceHours: 'service hours',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCPlaceWithoutReferencesMeta().fieldValueTranslations.de,
      ...new SCThingThatAcceptsPaymentsWithoutReferencesMeta().fieldValueTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<SCRoomCategories, SCRoomSpecificValues>()
        .fieldValueTranslations.de,
      ...new SCThingInPlaceMeta().fieldValueTranslations.de,
      categories: {
        'cafe': 'Café',
        'canteen': 'Kantine',
        'computer': 'PC-Pool',
        'education': 'Bildung',
        'laboratory': 'Labor',
        'learn': 'Lernen',
        'library': 'Bibliothek',
        'lounge': 'Lounge',
        'office': 'Büro',
        'restaurant': 'Restaurant',
        'restroom': 'Toilette',
        'student canteen': 'Mensa',
        'student union': 'Studentenvereinigung',
      },
      type: 'Raum',
    },
    en: {
      ...new SCPlaceWithoutReferencesMeta().fieldValueTranslations.en,
      ...new SCThingThatAcceptsPaymentsWithoutReferencesMeta().fieldValueTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<SCRoomCategories, SCRoomSpecificValues>()
        .fieldValueTranslations.en,
      ...new SCThingInPlaceMeta().fieldValueTranslations.en,
      type: SCThingType.Room,
    },
  };
}
