/*
 * Copyright (C) 2021-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {SCEvent, SCEventMeta, SCEventWithoutReferences} from './abstract/event';
import {SCThingMeta, SCThingTranslatableProperties, SCThingType} from './abstract/thing';

/**
 * An publication event without references
 */
export interface SCPublicationEventWithoutReferences extends SCEventWithoutReferences {
  /**
   * All the locations related to the event (e.g. where a creative work was published)
   */
  locations?: string[];

  /**
   * An organization (or a person) that is publishing at the event
   */
  publisher?: string;

  /**
   * Translated fields of an publication event
   */
  translations?: SCTranslations<SCPublicationEventTranslatableProperties>;

  /**
   * Type of an publication event
   */
  type: SCThingType.PublicationEvent;
}

/**
 * An publication event
 *
 * @validatable
 * @indexable
 */
export interface SCPublicationEvent extends SCEvent, SCPublicationEventWithoutReferences {
  /**
   * Translated fields of an publication event
   */
  translations?: SCTranslations<SCPublicationEventTranslatableProperties>;

  /**
   * Type of an publication event
   */
  type: SCThingType.PublicationEvent;
}

/**
 * Translatable properties of an publication event
 */
export type SCPublicationEventTranslatableProperties = SCThingTranslatableProperties;

/**
 * Meta information about publication events
 */
export class SCPublicationEventMeta extends SCThingMeta implements SCMetaTranslations<SCPublicationEvent> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCEventMeta().fieldTranslations.de,
      locations: 'Erscheinungsorte',
      publisher: 'Verlag',
    },
    en: {
      ...new SCEventMeta().fieldTranslations.en,
      locations: 'places of publication',
      publisher: 'publisher',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCEventMeta().fieldValueTranslations.de,
      type: 'Veröffentlichung',
    },
    en: {
      ...new SCEventMeta().fieldValueTranslations.en,
      type: 'publication event',
    },
  };
}
