/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {
  SCPlace,
  SCPlaceWithoutReferences,
  SCPlaceWithoutReferencesMeta,
  SCPlaceWithoutReferencesTranslatableProperties,
} from './abstract/place';
import {SCThingMeta, SCThingType} from './abstract/thing';
import {
  SCThingWithCategories,
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from './abstract/thing-with-categories';

export type SCBuildingCategories =
  | 'cafe'
  | 'education'
  | 'library'
  | 'office'
  | 'canteen'
  | 'student canteen'
  | 'restaurant'
  | 'restroom';

export interface SCBuildingWithoutReferences
  extends SCThingWithCategoriesWithoutReferences<SCBuildingCategories, SCThingWithCategoriesSpecificValues>,
    SCPlaceWithoutReferences {
  /**
   * List of floor names of the place
   *
   * @filterable
   * @keyword
   */
  floors?: string[];

  /**
   * Translated fields of a building
   */
  translations?: SCTranslations<SCBuildingTranslatableProperties>;

  /**
   * Type of the building
   */
  type: SCThingType.Building;
}

/**
 * A building
 *
 * @validatable
 * @indexable
 */
export interface SCBuilding
  extends SCBuildingWithoutReferences,
    SCPlace,
    SCThingWithCategories<SCBuildingCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * Translated fields of a building
   */
  translations?: SCTranslations<SCBuildingTranslatableProperties>;

  /**
   * Type of the building
   */
  type: SCThingType.Building;
}

export interface SCBuildingTranslatableProperties
  extends SCPlaceWithoutReferencesTranslatableProperties,
    SCThingWithCategoriesTranslatableProperties {
  /**
   * @see SCBuilding.floors
   */
  floors?: string[];
}

/**
 * Meta information about a place
 */
export class SCBuildingMeta extends SCThingMeta implements SCMetaTranslations<SCBuilding> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCBuildingCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.de,
      ...new SCPlaceWithoutReferencesMeta().fieldTranslations.de,
      floors: 'Etagen',
    },
    en: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCBuildingCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.en,
      ...new SCPlaceWithoutReferencesMeta().fieldTranslations.en,
      floors: 'floors',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCBuildingCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.de,
      ...new SCPlaceWithoutReferencesMeta().fieldValueTranslations.de,
      categories: {
        'cafe': 'Café',
        'canteen': 'Kantine',
        'education': 'Bildung',
        'library': 'Bibliothek',
        'office': 'Büro',
        'restaurant': 'Restaurant',
        'restroom': 'Toilette',
        'student canteen': 'Mensa',
      },
      type: 'Gebäude',
    },
    en: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCBuildingCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.en,
      ...new SCPlaceWithoutReferencesMeta().fieldValueTranslations.en,
      type: SCThingType.Building,
    },
  };
}
