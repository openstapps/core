/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {SCAcademicTermWithoutReferences} from './abstract/academic-term';
import {SCThing, SCThingMeta, SCThingType} from './abstract/thing';
import {
  SCThingWithCategories,
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from './abstract/thing-with-categories';

/**
 * A catalog without references
 */
export interface SCCatalogWithoutReferences
  extends SCThingWithCategoriesWithoutReferences<SCCatalogCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * Level of the catalog (0 for 'root catalog', 1 for its subcatalog, 2 for its subcatalog etc.)
   *
   * Needed for keeping order in catalog inheritance array.
   *
   * @filterable
   * @integer
   */
  level: number;

  /**
   * Type of a catalog
   */
  type: SCThingType.Catalog;
}

/**
 * A catalog
 *
 * @validatable
 * @indexable
 */
export interface SCCatalog
  extends SCCatalogWithoutReferences,
    SCThing,
    SCThingWithCategories<SCCatalogCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * Academic term that a catalog belongs to (e.g. semester)
   */
  academicTerm?: SCAcademicTermWithoutReferences;

  /**
   * The direct parent of a catalog
   */
  superCatalog?: SCCatalogWithoutReferences;

  /**
   * An array of catalogs from the 'level 0' (root) catalog to the direct parent
   */
  superCatalogs?: SCCatalogWithoutReferences[];

  /**
   * Translated fields of a catalog
   */
  translations?: SCTranslations<SCThingWithCategoriesTranslatableProperties>;

  /**
   * Type of a catalog
   */
  type: SCThingType.Catalog;
}

/**
 * Catalog meta data
 */
export class SCCatalogMeta extends SCThingMeta implements SCMetaTranslations<SCCatalog> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCCatalogCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.de,
      academicTerm: 'Semester',
      level: 'Ebene',
      superCatalog: 'übergeordnetes Verzeichniss',
      superCatalogs: 'übergeordnete Verzeichnisse',
    },
    en: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCCatalogCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.en,
      academicTerm: 'academic term',
      level: 'level',
      superCatalog: 'parent catalog',
      superCatalogs: 'parent catalogs',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCCatalogCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.de,
      categories: {
        'university events': 'Universitätsveranstaltung',
      },
      type: 'Verzeichnis',
    },
    en: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCCatalogCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.en,
      type: SCThingType.Catalog,
    },
  };
}

/**
 * Categories of catalogs
 */
export type SCCatalogCategories = 'university events';
