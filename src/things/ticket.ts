/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations} from '../general/i18n';
import {SCISO8601Duration} from '../general/time';
import {SCThingMeta, SCThingType, SCThingWithoutReferences} from './abstract/thing';
import {SCThingInPlace, SCThingInPlaceMeta} from './abstract/thing-in-place';

/**
 * A ticket without references
 */
export interface SCTicketWithoutReferences extends SCThingWithoutReferences {
  /**
   * Approximate wait time
   */
  approxWaitingTime: SCISO8601Duration;

  /**
   * Waiting number of the ticket
   *
   * @keyword
   */
  currentTicketNumber: string;

  /**
   * Service type of the ticket
   */
  serviceType: string;

  /**
   * Type of a ticket
   */
  type: SCThingType.Ticket;
}

/**
 * A ticket
 *
 * @validatable
 * @indexable
 */
export interface SCTicket extends SCTicketWithoutReferences, SCThingInPlace {
  /**
   * Type of a ticket
   */
  type: SCThingType.Ticket;
}

/**
 * Meta information about a ticket
 */
export class SCTicketMeta extends SCThingMeta implements SCMetaTranslations<SCTicket> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingInPlaceMeta().fieldTranslations.de,
      approxWaitingTime: 'ungefähre Wartezeit',
      currentTicketNumber: 'aktuelle Ticketnummer',
      serviceType: 'Service Kategorie',
    },
    en: {
      ...new SCThingInPlaceMeta().fieldTranslations.en,
      approxWaitingTime: 'approximate waiting time',
      currentTicketNumber: 'current ticket number',
      serviceType: 'type of service',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingInPlaceMeta().fieldValueTranslations.de,
      type: 'Ticket',
    },
    en: {
      ...new SCThingInPlaceMeta().fieldValueTranslations.en,
      type: SCThingType.Ticket,
    },
  };
}
