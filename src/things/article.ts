/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {
  SCCreativeWork,
  SCCreativeWorkMeta,
  SCCreativeWorkTranslatableProperties,
  SCCreativeWorkWithoutReferences,
} from './abstract/creative-work';
import {SCThingMeta, SCThingType} from './abstract/thing';
import {
  SCAcademicPriceGroup,
  SCThingThatCanBeOffered,
  SCThingThatCanBeOfferedMeta,
  SCThingThatCanBeOfferedTranslatableProperties,
  SCThingThatCanBeOfferedWithoutReferences,
} from './abstract/thing-that-can-be-offered';
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from './abstract/thing-with-categories';
import {SCPeriodicalWithoutReferences} from './periodical';

/**
 * Categories of an article
 */
export type SCArticleCategories = 'unipedia' | 'article' | 'eArticle';

/**
 * An article without references
 */
export interface SCArticleWithoutReferences
  extends SCCreativeWorkWithoutReferences,
    SCThingThatCanBeOfferedWithoutReferences,
    SCThingWithCategoriesWithoutReferences<SCArticleCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * Article itself as markdown
   *
   * @text
   */
  articleBody?: string;

  /**
   * Categories of an article
   */
  categories: SCArticleCategories[];

  /**
   * Translated fields of an article
   */
  translations?: SCTranslations<SCArticleTranslatableProperties>;

  /**
   * Type of an article
   */
  type: SCThingType.Article;
}

/**
 * An article
 *
 * @validatable
 * @indexable
 */
export interface SCArticle
  extends SCCreativeWork,
    SCThingThatCanBeOffered<SCAcademicPriceGroup>,
    SCArticleWithoutReferences {
  /**
   * A periodical to which this article belongs
   */
  isPartOf?: SCPeriodicalWithoutReferences;
  /**
   * Additional information about how to find the article inside of its "parent" (which is provided in 'isPartOf')
   * e.g. "40, 2011, S. 2-3"
   */
  reference?: string;
  /**
   * Translated fields of an article
   */
  translations?: SCTranslations<SCArticleTranslatableProperties>;

  /**
   * Type of an article
   */
  type: SCThingType.Article;
}

/**
 * Translatable properties of an article
 */
export interface SCArticleTranslatableProperties
  extends SCThingWithCategoriesTranslatableProperties,
    SCThingThatCanBeOfferedTranslatableProperties,
    SCCreativeWorkTranslatableProperties {
  /**
   * Translation of the article itself as markdown
   *
   * @text
   */
  articleBody?: string[];
}

/**
 * Meta information about an article
 */
export class SCArticleMeta extends SCThingMeta implements SCMetaTranslations<SCArticle> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCCreativeWorkMeta().fieldTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCArticleCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.de,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldTranslations.de,
      categories: 'Format',
      reference: 'Referenz',
      articleBody: 'Artikelinhalt',
    },
    en: {
      ...new SCCreativeWorkMeta().fieldTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCArticleCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.en,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldTranslations.en,
      categories: 'format',
      reference: 'reference',
      articleBody: 'article body',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCCreativeWorkMeta().fieldValueTranslations.de,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldValueTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCArticleCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.de,
      categories: {
        article: 'Artikel',
        eArticle: 'E-Aufsatz',
        unipedia: 'Unipedia',
      },
      type: 'Artikel',
    },
    en: {
      ...new SCCreativeWorkMeta().fieldValueTranslations.en,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldValueTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCArticleCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.en,
      type: SCThingType.Article,
      categories: {
        article: 'article',
        eArticle: 'E-Article',
        unipedia: 'unipedia',
      },
    },
  };
}
