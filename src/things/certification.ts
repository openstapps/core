/*
 * Copyright (C) 2023 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {SCThing, SCThingMeta, SCThingType} from './abstract/thing';
import {
  SCThingWithCategories,
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from './abstract/thing-with-categories';
import {SCOrganizationWithoutReferences} from './organization';

export interface SCCertificationWithoutReferences
  extends SCThingWithCategoriesWithoutReferences<
    SCCertificationCategories,
    SCThingWithCategoriesSpecificValues
  > {
  /**
   * A compact version of the image, for example for showing in list views
   */
  compactImage?: string;

  /**
   * Translations of a certification
   */
  translations?: SCTranslations<SCCertificationTranslatableProperties>;

  /**
   * Type of certification
   */
  type: SCThingType.Certification;
}

/**
 * @indexable
 * @validatable
 */
export interface SCCertification
  extends SCCertificationWithoutReferences,
    SCThing,
    SCThingWithCategories<SCCertificationCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * The authority responsible for issuing this certification
   */
  certificationAuthority?: SCOrganizationWithoutReferences;

  /**
   * Translations of a certification
   */
  translations?: SCTranslations<SCCertificationTranslatableProperties>;

  /**
   * Type of certification
   */
  type: SCThingType.Certification;
}

export interface SCCertificationTranslatableProperties extends SCThingWithCategoriesTranslatableProperties {
  image?: string;
  compactImage?: string;
}

export type SCCertificationCategories =
  | 'water consumption'
  | 'animal welfare'
  | 'climate impact'
  | 'rainforest protection';

export class SCCertificationMeta extends SCThingMeta implements SCMetaTranslations<SCCertification> {
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCCertificationCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.de,
      compactImage: 'Kompaktes Bild',
      certificationAuthority: 'Zertifizierungsstelle',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCCertificationCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.en,
      compactImage: 'compact image',
      certificationAuthority: 'certification authority',
    },
  };

  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCCertificationCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.de,
      type: 'Zertifizierung',
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCCertificationCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.en,
      type: SCThingType.Certification,
    },
  };
}
