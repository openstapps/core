/*
 * Copyright (C) 2021-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {SCISO8601Date} from '../general/time';
import {SCThing, SCThingMeta, SCThingType} from './abstract/thing';
import {
  SCThingWithCategories,
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from './abstract/thing-with-categories';
import {SCCourseOfStudyWithoutReferences} from './course-of-study';

/**
 * Categories of assessments
 */
export type SCAssessmentCategories = 'university assessment';

/**
 * An assessment without references
 *
 */
export interface SCAssessmentWithoutReferences
  extends SCThingWithCategoriesWithoutReferences<
    SCAssessmentCategories,
    SCThingWithCategoriesSpecificValues
  > {
  /**
   * Number of attempts
   *
   * @integer
   */
  attempt?: number;

  /**
   * Date assessment was taken or graded
   */
  date?: SCISO8601Date;

  /**
   * ECTS (credit-points)
   *
   * @float
   */
  ects?: number;

  /**
   * Grade
   */
  grade: string;

  /**
   * Current status
   */
  status?: string;

  /**
   * Translated fields of an assessment
   */
  translations?: SCTranslations<SCAssessmentTranslatableProperties>;

  /**
   * Type of an assessment
   */
  type: SCThingType.Assessment;
}

/**
 * An assessment
 *
 * @validatable
 */
export interface SCAssessment
  extends SCAssessmentWithoutReferences,
    SCThing,
    SCThingWithCategories<SCAssessmentCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * Course of study the assessment was taken for
   */
  courseOfStudy?: SCCourseOfStudyWithoutReferences;

  /**
   * An array of assessments from the 'level 0' (root) assessment to the direct parent
   */
  superAssessments?: SCAssessmentWithoutReferences[];

  /**
   * Translated fields of an assessment
   */
  translations?: SCTranslations<SCAssessmentTranslatableProperties>;

  /**
   * Type of an assessment
   */
  type: SCThingType.Assessment;
}

export interface SCAssessmentTranslatableProperties extends SCThingWithCategoriesTranslatableProperties {
  /**
   * @see SCAssessmentWithoutReferences.status
   */
  status?: string;
}

/**
 * Study module meta data
 */
export class SCAssessmentMeta extends SCThingMeta implements SCMetaTranslations<SCAssessment> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCAssessmentCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.de,
      attempt: 'Versuch',
      courseOfStudy: 'Studiengang',
      date: 'Datum',
      ects: 'ECTS-Punkte',
      grade: 'Note',
      status: 'Status',
      superAssessments: 'übergeordnete Prüfungen',
    },
    en: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCAssessmentCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.en,
      attempt: 'attempt',
      courseOfStudy: 'course of study',
      date: 'date',
      ects: 'ECTS points',
      grade: 'grade',
      status: 'status',
      superAssessments: 'parent assessments',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCAssessmentCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.de,
      type: 'Prüfung',
    },
    en: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCAssessmentCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.en,
      type: SCThingType.Assessment,
    },
  };
}
