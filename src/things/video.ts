/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCLanguage, SCMetaTranslations, SCTranslations} from '../general/i18n';
import {SCISO8601Duration} from '../general/time';
import {
  SCCreativeWork,
  SCCreativeWorkMeta,
  SCCreativeWorkTranslatableProperties,
  SCCreativeWorkWithoutReferences,
} from './abstract/creative-work';
import {SCThingMeta, SCThingType} from './abstract/thing';
import {
  SCAcademicPriceGroup,
  SCThingThatCanBeOffered,
  SCThingThatCanBeOfferedMeta,
  SCThingThatCanBeOfferedTranslatableProperties,
  SCThingThatCanBeOfferedWithoutReferences,
} from './abstract/thing-that-can-be-offered';
import {SCPersonWithoutReferences} from './person';

/**
 * A video without references
 */
export interface SCVideoWithoutReferences
  extends SCCreativeWorkWithoutReferences,
    SCThingThatCanBeOfferedWithoutReferences {
  /**
   * The Duration of the Video
   */
  duration?: SCISO8601Duration;

  /**
   * Downloadable/Streamable Files of the Video
   */
  sources?: SCVideoSource[];

  /**
   * URLs to a thumbnails for the Video
   *
   * @keyword
   */
  thumbnails?: string[];

  /**
   * Track Files for the Video
   */
  tracks?: SCVideoTrack;

  /**
   * A Transcript of the Video
   *
   * @text
   */
  transcript?: string;

  /**
   * Translated fields of a video
   */
  translations?: SCTranslations<SCVideoTranslatableFields>;

  /**
   * Type of an Video
   */
  type: SCThingType.Video;
}

export interface SCVideoSource {
  /**
   * Pixel Height of the Video
   *
   * @integer
   */
  height?: number;

  /**
   * MIME-Type of the source File
   *
   * @filterable
   */
  mimeType: SCVideoMimeType;

  /**
   * Size of the Video File in bytes
   *
   * @integer
   */
  size?: number;

  /**
   * URL to the Video File
   *
   * @keyword
   */
  url: string;

  /**
   * Pixel Width of the Video
   *
   * @integer
   */
  width?: number;
}

export interface SCVideoTrack {
  /**
   * Language of the Subtitle
   */
  language: SCLanguage;

  /**
   * Content Type of the Track File
   *
   * @filterable
   */
  type: SCVideoTrackTypes;

  /**
   * URL to the Track File
   *
   * @keyword
   */
  url: string;
}

/**
 * A video
 *
 * @validatable
 * @indexable
 */
export interface SCVideo
  extends SCCreativeWork,
    SCThingThatCanBeOffered<SCAcademicPriceGroup>,
    SCVideoWithoutReferences {
  /**
   * Persons acting in the Video
   */
  actors?: SCPersonWithoutReferences[];

  /**
   * Translated fields of a video
   */
  translations?: SCTranslations<SCVideoTranslatableFields>;

  /**
   * Type of a video
   */
  type: SCThingType.Video;
}

/**
 * Translatable properties of a video
 */
export interface SCVideoTranslatableFields
  extends SCCreativeWorkTranslatableProperties,
    SCThingThatCanBeOfferedTranslatableProperties {}

/**
 * Meta information about a video
 */
export class SCVideoMeta extends SCThingMeta implements SCMetaTranslations<SCVideo> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCCreativeWorkMeta().fieldTranslations.de,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldTranslations.de,
      actors: 'Darsteller',
      duration: 'Dauer',
      sources: 'Quellen',
      thumbnails: 'Vorschaubilder',
      tracks: 'Spuren',
      transcript: 'Transkript',
    },
    en: {
      ...new SCCreativeWorkMeta().fieldTranslations.en,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldTranslations.en,
      actors: 'actors',
      duration: 'duration',
      sources: 'sources',
      thumbnails: 'thumbnails',
      tracks: 'tracks',
      transcript: 'transcript',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCCreativeWorkMeta().fieldValueTranslations.de,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldValueTranslations.de,
      type: 'Video',
    },
    en: {
      ...new SCCreativeWorkMeta().fieldValueTranslations.en,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldValueTranslations.en,
      type: SCThingType.Video,
    },
  };
}

/**
 * Video Encoding Formats
 */
export type SCVideoMimeType =
  | 'video/mp4'
  | 'video/ogg'
  | 'video/webm'
  | 'application/vnd.apple.mpegurl'
  | 'application/dash+xml';

/**
 * Video Track Types
 */
export type SCVideoTrackTypes = 'closed captions' | 'chapters' | 'description' | 'metadata' | 'subtitles';
