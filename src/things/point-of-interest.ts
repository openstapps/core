/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {SCPlace, SCPlaceWithoutReferences, SCPlaceWithoutReferencesMeta} from './abstract/place';
import {SCThingMeta, SCThingType} from './abstract/thing';
import {SCThingInPlace, SCThingInPlaceMeta} from './abstract/thing-in-place';
import {
  SCThingWithCategories,
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from './abstract/thing-with-categories';

/**
 * A point of interest without references
 */
export interface SCPointOfInterestWithoutReferences
  extends SCThingWithCategoriesWithoutReferences<
      SCPointOfInterestCategories,
      SCThingWithCategoriesSpecificValues
    >,
    SCPlaceWithoutReferences {
  /**
   * Translated properties of a point of interest
   */
  translations?: SCTranslations<SCThingWithCategoriesTranslatableProperties>;

  /**
   * Type of a point of interest
   */
  type: SCThingType.PointOfInterest;
}

/**
 * A point of interest
 *
 * @validatable
 * @indexable
 */
export interface SCPointOfInterest
  extends SCPointOfInterestWithoutReferences,
    SCThingInPlace,
    SCPlace,
    SCThingWithCategories<SCPointOfInterestCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * Translated properties of a point of interest
   */
  translations?: SCTranslations<SCThingWithCategoriesTranslatableProperties>;

  /**
   * Type of a point of interest
   */
  type: SCThingType.PointOfInterest;
}

/**
 * Categories of a point of interest
 */
export type SCPointOfInterestCategories =
  | 'computer'
  | 'validator'
  | 'card charger'
  | 'printer'
  | 'disabled access';

/**
 * Meta information about points of interest
 */
export class SCPointOfInterestMeta extends SCThingMeta implements SCMetaTranslations<SCPointOfInterest> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCPointOfInterestCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.de,
      ...new SCPlaceWithoutReferencesMeta().fieldTranslations.de,
      ...new SCThingInPlaceMeta().fieldTranslations.de,
    },
    en: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCPointOfInterestCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.en,
      ...new SCPlaceWithoutReferencesMeta().fieldTranslations.en,
      ...new SCThingInPlaceMeta().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCPointOfInterestCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.de,
      ...new SCPlaceWithoutReferencesMeta().fieldValueTranslations.de,
      categories: {
        'card charger': 'Kartenaufwerter',
        'computer': 'Computer',
        'disabled access': 'barrierefreier Zugang',
        'printer': 'Drucker',
        'validator': 'Validierer',
      },
      type: 'Sonderziel',
    },
    en: {
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCPointOfInterestCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.en,
      ...new SCPlaceWithoutReferencesMeta().fieldValueTranslations.en,
      type: SCThingType.PointOfInterest,
    },
  };
}
