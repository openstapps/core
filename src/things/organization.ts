/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations} from '../general/i18n';
import {SCThingMeta, SCThingType, SCThingWithoutReferences} from './abstract/thing';
import {SCThingInPlace, SCThingInPlaceMeta} from './abstract/thing-in-place';
import {SCContactPointWithoutReferences} from './contact-point';

/**
 * An organization without references
 */
export interface SCOrganizationWithoutReferences extends SCThingWithoutReferences {
  /**
   * Type of an organization
   */
  type: SCThingType.Organization;
}

/**
 * An organization
 *
 * @validatable
 * @indexable
 */
export interface SCOrganization extends SCOrganizationWithoutReferences, SCThingInPlace {
  /**
   * A list of contact points concerning the organization
   */
  contactPoints?: SCContactPointWithoutReferences[];

  /**
   * Type of an organization
   */
  type: SCThingType.Organization;
}

/**
 * Meta information about an organization
 */
export class SCOrganizationMeta extends SCThingMeta implements SCMetaTranslations<SCOrganization> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingInPlaceMeta().fieldTranslations.de,
      contactPoints: 'Kontaktinformationen',
    },
    en: {
      ...new SCThingInPlaceMeta().fieldTranslations.en,
      contactPoints: 'contact details',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingInPlaceMeta().fieldValueTranslations.de,
      type: 'Einrichtung',
    },
    en: {
      ...new SCThingInPlaceMeta().fieldValueTranslations.en,
      type: SCThingType.Organization,
    },
  };
}
