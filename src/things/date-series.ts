/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {SCISO8601Date, SCISO8601Duration} from '../general/time';
import {SCThingMeta, SCThingType} from './abstract/thing';
import {SCThingInPlace, SCThingInPlaceMeta} from './abstract/thing-in-place';
import {
  SCAcademicPriceGroup,
  SCThingThatCanBeOffered,
  SCThingThatCanBeOfferedMeta,
  SCThingThatCanBeOfferedTranslatableProperties,
  SCThingThatCanBeOfferedWithoutReferences,
} from './abstract/thing-that-can-be-offered';
import {SCAcademicEventWithoutReferences} from './academic-event';
import {SCPersonWithoutReferences} from './person';
import {SCSportCourseWithoutReferences} from './sport-course';

/**
 * Price groups for sport courses
 */
export interface SCSportCoursePriceGroup extends SCAcademicPriceGroup {
  /**
   * Price for alumnis
   *
   * @float
   */
  alumni?: number;
}

/**
 * A date without references
 */
export interface SCDateSeriesWithoutReferences extends SCThingThatCanBeOfferedWithoutReferences {
  /**
   * Dates of the date series that are initially planned to be held
   *
   * @filterable
   */
  dates: SCISO8601Date[];

  /**
   * Duration of the date series
   */
  duration: SCISO8601Duration;

  /**
   * Dates that where initially planned to be held but are cancelled
   */
  exceptions?: SCISO8601Date[];

  /**
   * Frequency of the date series
   *
   * @filterable
   */
  repeatFrequency?: SCISO8601Duration;

  /**
   * Translated properties
   */
  translations?: SCTranslations<SCDateSeriesTranslatableProperties>;

  /**
   * Type of a date series
   */
  type: SCThingType.DateSeries;
}

/**
 * A date series
 *
 * @validatable
 * @indexable
 */
export interface SCDateSeries
  extends SCDateSeriesWithoutReferences,
    SCThingInPlace,
    SCThingThatCanBeOffered<SCSportCoursePriceGroup> {
  /**
   * Event to which the date series belongs
   */
  event: SCAcademicEventWithoutReferences | SCSportCourseWithoutReferences;

  /**
   * Performers of the date series
   */
  performers?: SCPersonWithoutReferences[];

  /**
   * Translated properties
   */
  translations?: SCTranslations<SCDateSeriesTranslatableProperties>;

  /**
   * Type of a date series
   */
  type: SCThingType.DateSeries;
}

/**
 * Meta information about a date series
 */
export class SCDateSeriesMeta extends SCThingMeta implements SCMetaTranslations<SCDateSeries> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingInPlaceMeta().fieldTranslations.de,
      ...new SCThingThatCanBeOfferedMeta<SCSportCoursePriceGroup>().fieldTranslations.de,
      dates: 'Einzeltermine',
      duration: 'Dauer',
      event: 'Event',
      exceptions: 'Ausnahmen',
      repeatFrequency: 'Frequenz',
      performers: 'Vortragende',
    },
    en: {
      ...new SCThingInPlaceMeta().fieldTranslations.en,
      ...new SCThingThatCanBeOfferedMeta<SCSportCoursePriceGroup>().fieldTranslations.en,
      dates: 'dates',
      duration: 'duration',
      event: 'event',
      exceptions: 'exceptions',
      repeatFrequency: 'frequency',
      performers: 'performers',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingInPlaceMeta().fieldValueTranslations.de,
      ...new SCThingThatCanBeOfferedMeta<SCSportCoursePriceGroup>().fieldValueTranslations.de,
      type: 'Terminserie',
    },
    en: {
      ...new SCThingInPlaceMeta().fieldValueTranslations.en,
      ...new SCThingThatCanBeOfferedMeta<SCSportCoursePriceGroup>().fieldValueTranslations.en,
      type: SCThingType.DateSeries,
    },
  };
}

/**
 * Translatable properties of date series'
 */
export type SCDateSeriesTranslatableProperties = SCThingThatCanBeOfferedTranslatableProperties;
