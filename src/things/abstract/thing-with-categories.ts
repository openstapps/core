/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../../general/i18n';
import {SCMap} from '../../general/map';
import {SCThing, SCThingMeta, SCThingTranslatableProperties, SCThingWithoutReferences} from './thing';

/**
 * A thing without references with categories
 *
 * !!! BEWARE !!!
 * `T` should be a string literal union type - e.g. `T = 'foo' | 'bar' | 'foobar';`
 */
export interface SCThingWithCategoriesWithoutReferences<T, U extends SCThingWithCategoriesSpecificValues>
  extends SCThingWithoutReferences {
  /**
   * Categories of a thing with categories
   *
   * @sortable ducet
   * @aggregatable
   * @filterable
   */
  categories: T[];

  /**
   * Use this to explicitly override general opening hours brought in scope by openingHoursSpecification or openingHours
   *
   * A map from categories to their specific values.
   */
  categorySpecificValues?: SCMap<U>;

  /**
   * Translated fields of a thing with categories
   */
  translations?: SCTranslations<SCThingWithCategoriesTranslatableProperties>;
}

/**
 * A thing with categories
 */
export interface SCThingWithCategories<T, U extends SCThingWithCategoriesSpecificValues>
  extends SCThing,
    SCThingWithCategoriesWithoutReferences<T, U> {
  /**
   * Translated fields of a thing with categories
   */
  translations?: SCTranslations<SCThingWithCategoriesTranslatableProperties>;
}

/**
 * Translatable properties of a thing with categories
 */
export interface SCThingWithCategoriesTranslatableProperties extends SCThingTranslatableProperties {
  /**
   * translations of the categories of a thing with categories
   *
   * @sortable ducet
   */
  categories?: string[];
}

/**
 * Category specific values of a thing with categories
 *
 * This interface contains properties that can be specific to a certain category.
 */
export interface SCThingWithCategoriesSpecificValues {
  /**
   * Category specific alternate names of a thing
   *
   * @keyword
   */
  alternateNames?: string[];

  /**
   * Category specific description of a thing
   *
   * @text
   */
  description?: string;

  /**
   * URL of a category specific image of a thing
   *
   * @keyword
   */
  image?: string;

  /**
   * Category specific name of a thing
   *
   * @sortable ducet
   * @text
   */
  name?: string;

  /**
   * Category specific URL of a thing
   *
   * @keyword
   */
  url?: string;
}

/**
 * Meta information about a thing without references that accepts payments
 * It intentionally does not extend the SCThingMeta implementation to be able to include generics.
 */
export class SCThingWithCategoriesWithoutReferencesMeta<T, U extends SCThingWithCategoriesSpecificValues>
  implements SCMetaTranslations<SCThingWithCategoriesWithoutReferences<T, U>>
{
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      categories: 'Kategorien',
      categorySpecificValues: 'besondere Kategorien',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      categories: 'categories',
      categorySpecificValues: 'category with specific values',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
    },
  };
}
