/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCLanguageCode, SCMetaTranslations, SCTranslations} from '../../general/i18n';
import {SCISO8601Date} from '../../general/time';
import {SCOrganizationWithoutReferences} from '../organization';
import {SCPersonWithoutReferences} from '../person';
import {SCPublicationEventWithoutReferences} from '../publication-event';
import {SCThing, SCThingMeta, SCThingTranslatableProperties, SCThingWithoutReferences} from './thing';

/**
 * A creative work without references
 */
export interface SCCreativeWorkWithoutReferences extends SCThingWithoutReferences {
  /**
   * Languages this creative work is available in
   */
  availableLanguages?: SCLanguageCode[];

  /**
   * Date the creative work was published
   */
  datePublished?: SCISO8601Date;

  /**
   * Edition of a creative work (e.g. the book edition or edition of an article)
   *
   * @keyword
   */
  edition?: string;

  /**
   * Date (in text form) the creative work was published for the first time
   *
   * @keyword
   */
  firstPublished?: string;

  /**
   * Languages this creative work is written/recorded/... in
   *
   * @filterable
   */
  inLanguage?: SCLanguageCode;

  /**
   * Keywords of the creative work
   *
   * @aggregatable
   * @filterable
   * @keyword
   */
  keywords?: string[];

  /**
   * Date (in text form) the creative work was most recently
   *
   * @keyword
   */
  lastPublished?: string;

  /**
   * Translated fields of the creative work
   */
  translations?: SCTranslations<SCCreativeWorkTranslatableProperties>;
}

/**
 * A creative work
 */
export interface SCCreativeWork extends SCCreativeWorkWithoutReferences, SCThing {
  /**
   * Authors of the creative work
   */
  authors?: SCPersonWithoutReferences[];

  /**
   * A creative work to which the creative work belongs
   */
  isPartOf?: SCCreativeWorkWithoutReferences;

  /**
   * List of events at which the creative work was published
   */
  publications?: SCPublicationEventWithoutReferences[];

  /**
   * List of publishers of the creative work
   */
  publishers?: Array<SCPersonWithoutReferences | SCOrganizationWithoutReferences>;

  /**
   * A text representing on organization on whose behalf the creator was working
   */
  sourceOrganization?: string;

  /**
   * Translated fields of the creative work
   */
  translations?: SCTranslations<SCCreativeWorkTranslatableProperties>;
}

/**
 * Translatable properties of creative works
 */
export interface SCCreativeWorkTranslatableProperties extends SCThingTranslatableProperties {
  /**
   * Translation of the keywords of the creative work
   *
   * @keyword
   */
  keywords?: string[];
}

/**
 * Meta information about creative works
 */
export class SCCreativeWorkMeta extends SCThingMeta implements SCMetaTranslations<SCCreativeWork> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      name: 'Titel',
      authors: 'beteiligte Personen',
      availableLanguages: 'verfügbare Übersetzungen',
      datePublished: 'Veröffentlichungsdatum',
      edition: 'Ausgabe',
      firstPublished: 'erste Veröffentlichung',
      inLanguage: 'Inhaltssprache',
      isPartOf: 'erschienen in',
      keywords: 'Schlagwörter',
      lastPublished: 'aktuellste Veröffentlichung',
      publishers: 'Verleger',
      publications: 'Veröffentlichungen',
      sourceOrganization: 'Körperschaft',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      name: 'title',
      authors: 'involved persons',
      availableLanguages: 'available languages',
      datePublished: 'release date',
      edition: 'edition',
      firstPublished: 'first published',
      inLanguage: 'content language',
      isPartOf: 'published in',
      keywords: 'keywords',
      lastPublished: 'last published',
      publishers: 'publishers',
      publications: 'publications',
      sourceOrganization: 'corporation',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
    },
  };
}
