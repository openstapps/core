/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations} from '../../general/i18n';
import {SCThing, SCThingMeta, SCThingWithoutReferences} from './thing';

/**
 * Types of payment that are accepted at a place.
 */
export type SCThingThatAcceptsPaymentsAcceptedPayments = 'cash' | 'credit' | 'cafeteria card';

/**
 * A thing without references that accepts payments
 */
export interface SCThingThatAcceptsPaymentsWithoutReferences extends SCThingWithoutReferences {
  /**
   * Accepted payments of the place
   *
   * @filterable
   */
  paymentsAccepted?: SCThingThatAcceptsPaymentsAcceptedPayments[];
}

/**
 * A thing that accepts payments
 */
export interface SCThingThatAcceptsPayments extends SCThingThatAcceptsPaymentsWithoutReferences, SCThing {
  // noop
}

/**
 * Meta information about a thing without references that accepts payments
 */
export class SCThingThatAcceptsPaymentsWithoutReferencesMeta
  extends SCThingMeta
  implements SCMetaTranslations<SCThingThatAcceptsPaymentsWithoutReferences>
{
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      paymentsAccepted: 'Bezahlmethoden',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      paymentsAccepted: 'accepted payment methods',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
      paymentsAccepted: {
        'cafeteria card': 'Mensakarte',
        'cash': 'Bar',
        'credit': 'Kreditkarte',
      },
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
    },
  };
}
