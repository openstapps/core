/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations} from '../../general/i18n';
import {SCInPlace} from './place';
import {SCThing, SCThingMeta} from './thing';

/**
 * A thing that is or happens in a place
 */
export interface SCThingInPlace extends SCThing, SCInPlace {
  // noop
}

/**
 * Meta information about thing in a place
 */
export class SCThingInPlaceMeta extends SCThingMeta implements SCMetaTranslations<SCThingInPlace> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      inPlace: 'Ort',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      inPlace: 'location',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
    },
  };
}
