/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations} from '../../general/i18n';
import {SCThing, SCThingMeta, SCThingWithoutReferences} from './thing';

/**
 * An academic degree without references
 */
export interface SCAcademicDegreeWithoutReferences extends SCThingWithoutReferences {
  /**
   * The achievable academic degree
   *
   * @filterable
   * @sortable ducet
   */
  academicDegree: string;

  /**
   * The achievable academic degree with academic field specification
   * (eg. Master of Science)
   *
   */
  academicDegreewithField?: string;

  /**
   * The achievable academic degree with academic field specification
   * shorted (eg. M.Sc.).
   *
   */
  academicDegreewithFieldShort?: string;
}

/**
 * An academic degree
 */
export interface SCAcademicDegree extends SCAcademicDegreeWithoutReferences, SCThing {
  // noop
}

/**
 * Meta information about academic degrees
 */
export class SCAcademicDegreeMeta extends SCThingMeta implements SCMetaTranslations<SCAcademicDegree> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      academicDegree: 'Abschlussgrad',
      academicDegreewithField: 'Abschlussbezeichnung',
      academicDegreewithFieldShort: 'Abschlussbezeichnung (kurz)',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      academicDegree: 'academic degree',
      academicDegreewithField: 'acedemic degree and discipline',
      academicDegreewithFieldShort: 'acedemic degree and discipline (short)',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
    },
  };
}
