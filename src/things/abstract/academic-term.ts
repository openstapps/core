/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations} from '../../general/i18n';
import {SCISO8601Date} from '../../general/time';
import {SCThing, SCThingMeta, SCThingWithoutReferences} from './thing';

/**
 * An academic term without references
 */
export interface SCAcademicTermWithoutReferences extends SCThingWithoutReferences {
  /**
   * Short name of the academic term, using the given pattern
   *
   * @aggregatable
   * @filterable
   * @keyword
   */
  acronym: string;

  /**
   * End date of the academic term
   *
   * @filterable
   */
  endDate: SCISO8601Date;

  /**
   * End date of lectures in the academic term
   *
   * @filterable
   */
  eventsEndDate?: SCISO8601Date;

  /**
   * Start date of lectures in the academic term
   *
   * @filterable
   */
  eventsStartDate?: SCISO8601Date;

  /**
   * Start date of the academic term
   *
   * @filterable
   */
  startDate: SCISO8601Date;
}

/**
 * An academic term
 */
export interface SCAcademicTerm extends SCAcademicTermWithoutReferences, SCThing {
  // noop
}

/**
 * Meta information about academic terms
 */
export class SCAcademicTermWithoutReferencesMeta extends SCThingMeta implements SCMetaTranslations<SCThing> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      acronym: 'Akronym',
      endDate: 'Enddatum',
      eventsEndDate: 'Enddatum der Veranstaltungen',
      eventsStartDate: 'Startdatum der Veranstaltungen',
      startDate: 'Startdatum',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      acronym: 'acronym',
      endDate: 'end date',
      eventsEndDate: 'end date of events',
      eventsStartDate: 'start date of events',
      startDate: 'start date',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
    },
  };
}
