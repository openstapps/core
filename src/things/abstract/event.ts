/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations} from '../../general/i18n';
import {SCCatalogWithoutReferences} from '../catalog';
import {SCPersonWithoutReferences} from '../person';
import {SCSemesterWithoutReferences} from '../semester';
import {SCCreativeWorkWithoutReferences} from './creative-work';
import {SCThing, SCThingMeta, SCThingWithoutReferences} from './thing';

/**
 * An event without references
 */
export interface SCEventWithoutReferences extends SCThingWithoutReferences {
  /**
   * Maximum number of participants of the event
   *
   * A maximum number of people that can participate in the event.
   *
   * @integer
   */
  maximumParticipants?: number;

  /**
   * Remaining attendee capacity of the event
   *
   * This number represents the remaining open spots.
   *
   * @integer
   */
  remainingAttendeeCapacity?: number;
}

/**
 * An event
 */
export interface SCEvent extends SCEventWithoutReferences, SCThing {
  /**
   * Academic terms that an event belongs to, e.g. semester(s).
   */
  academicTerms?: SCSemesterWithoutReferences[];

  /**
   * Catalogs to which an event belongs
   */
  catalogs?: SCCatalogWithoutReferences[];

  /**
   * A list of creative works that are associated with this event
   *
   * This can be recommended books, CDs that can be bought, etc.
   */
  creativeWorks?: SCCreativeWorkWithoutReferences[];

  /**
   * Organizers of the event
   */
  organizers?: SCPersonWithoutReferences[];

  /**
   * Performers of the event
   */
  performers?: SCPersonWithoutReferences[];
}

/**
 * Meta information about events
 */
export class SCEventMeta extends SCThingMeta implements SCMetaTranslations<SCEvent> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      academicTerms: 'Semester',
      catalogs: 'Verzeichnis',
      creativeWorks: 'begleitende Werke',
      maximumParticipants: 'maximale Anzahl an Teilnehmern',
      organizers: 'Origanisatoren',
      performers: 'Vortragende',
      remainingAttendeeCapacity: 'verfügbare Anzahl an Teilnehmern',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      academicTerms: 'academic terms',
      catalogs: 'catalogs',
      creativeWorks: 'related material',
      maximumParticipants: 'maximum participants',
      organizers: 'organizers',
      performers: 'performers',
      remainingAttendeeCapacity: 'remaining attendee capacity',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
    },
  };
}
