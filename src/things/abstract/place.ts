/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {Point, Polygon} from 'geojson';
import {SCMetaTranslations, SCTranslations} from '../../general/i18n';
import {SCBuildingWithoutReferences} from '../building';
import {SCPointOfInterestWithoutReferences} from '../point-of-interest';
import {SCRoomWithoutReferences} from '../room';
import {SCThing, SCThingMeta, SCThingTranslatableProperties, SCThingWithoutReferences} from './thing';

/**
 * Positional information
 */
export interface SCGeoInformation {
  /**
   * Center point of a place
   */
  point: Point;
  /**
   * Shape of a place
   */
  polygon?: Polygon;
}

/**
 * A postal address
 */
export interface SCPostalAddress {
  /**
   * Country of the address
   *
   * @filterable
   */
  addressCountry: string;

  /**
   * City of the address
   *
   * @filterable
   */
  addressLocality: string;

  /**
   * State of the address
   *
   * @filterable
   */
  addressRegion?: string;

  /**
   * Zip code of the address
   *
   * @filterable
   */
  postalCode: string;

  /**
   * Optional post box number
   *
   * @filterable
   */
  postOfficeBoxNumber?: string;

  /**
   * Street of the address - with house number!
   *
   * @filterable
   */
  streetAddress: string;
}

/**
 * A place without references
 */
export interface SCPlaceWithoutReferences extends SCThingWithoutReferences {
  /**
   * Address of the place
   */
  address?: SCPostalAddress;

  /**
   * Positional information of the place
   *
   * !!! BEWARE !!!
   * Can not be a GeometryCollection because ElasticSearch does not allow distance filtering/sorting on other types
   */
  geo: SCGeoInformation;

  /**
   * Opening hours of the place
   *
   * @see http://wiki.openstreetmap.org/wiki/Key:opening_hours/specification
   * @keyword
   */
  openingHours?: string;

  /**
   * Translated fields of a place
   */
  translations?: SCTranslations<SCPlaceWithoutReferencesTranslatableProperties>;
}

/**
 * A place
 */
export interface SCPlace extends SCPlaceWithoutReferences, SCThing {
  /**
   * Translated fields of a place
   */
  translations?: SCTranslations<SCPlaceWithoutReferencesTranslatableProperties>;
}

/**
 * Translatable properties of a place without references
 */
export interface SCPlaceWithoutReferencesTranslatableProperties extends SCThingTranslatableProperties {
  /**
   * Address of a place
   */
  address?: SCPostalAddress;
}

/**
 * Meta information about creative works
 */
export class SCPlaceWithoutReferencesMeta
  extends SCThingMeta
  implements SCMetaTranslations<SCPlaceWithoutReferences>
{
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      address: 'Adresse',
      geo: 'Geoinformation',
      openingHours: 'Öffnungszeiten',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      address: 'address',
      geo: 'geographic information',
      openingHours: 'opening hours',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
    },
  };
}

/**
 * Something that is or happens in a place
 *
 * !Important!
 * This is not a SCThing.
 */
export interface SCInPlace {
  /**
   * Place the thing is or happens in
   */
  inPlace?: SCBuildingWithoutReferences | SCPointOfInterestWithoutReferences | SCRoomWithoutReferences;
}
