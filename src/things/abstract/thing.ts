/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../../general/i18n';
import {SCMap} from '../../general/map';
import {SCISO8601Date} from '../../general/time';
import {SCUuid} from '../../general/uuid';
import {SCOrganizationWithoutReferences} from '../organization';
import {SCPersonWithoutReferences} from '../person';

/**
 * Types a thing can be
 */
export enum SCThingType {
  Assessment = 'assessment',
  AcademicEvent = 'academic event',
  Article = 'article',
  Book = 'book',
  Periodical = 'periodical',
  Building = 'building',
  Catalog = 'catalog',
  Certification = 'certification',
  ContactPoint = 'contact point',
  CourseOfStudy = 'course of study',
  DateSeries = 'date series',
  Diff = 'diff',
  Dish = 'dish',
  Favorite = 'favorite',
  Floor = 'floor',
  Message = 'message',
  Organization = 'organization',
  Person = 'person',
  PointOfInterest = 'point of interest',
  PublicationEvent = 'publication event',
  Room = 'room',
  Semester = 'semester',
  Setting = 'setting',
  SportCourse = 'sport course',
  StudyModule = 'study module',
  Ticket = 'ticket',
  ToDo = 'todo',
  Tour = 'tour',
  Video = 'video',
}

/**
 * A thing without references
 */
export interface SCThingWithoutReferences {
  /**
   * Alternate names of the thing
   *
   * @filterable
   * @keyword
   */
  alternateNames?: string[];

  /**
   * Description of the thing
   *
   * @minLength 1
   * @text
   */
  description?: string;

  /**
   * The identifier property represents any kind of additional identifier for any kind of SCThing
   *
   * E.g. GTIN codes, UUIDs, Database IDs etc.
   */
  identifiers?: SCMap<string>;

  /**
   * URL of an image of the thing
   *
   * @keyword
   */
  image?: string;

  /**
   * Name of the thing
   *
   * @filterable
   * @minLength 1
   * @sortable ducet
   * @text
   */
  name: string;

  /**
   * URL of a reference Web page that unambiguously indicates the item's identity
   *
   * E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website.
   */
  sameAs?: string;

  /**
   * Translations of specific values of the object
   *
   * Take precedence over "main" value for selected languages.
   */
  translations?: SCTranslations<SCThingTranslatableProperties>;

  /**
   * Type of the thing
   *
   * @sortable ducet
   * @filterable
   * @aggregatable global
   */
  type: SCThingType;

  /**
   * Universally unique identifier of the thing
   */
  uid: SCUuid;
}

/**
 * A thing
 */
export interface SCThing extends SCThingWithoutReferences {
  /**
   * Origin of the thing
   */
  origin: SCThingRemoteOrigin | SCThingUserOrigin;
}

/**
 * Possible types of an origin
 */
export enum SCThingOriginType {
  Remote = 'remote',
  User = 'user',
}

/**
 * Origin of a thing
 */
export interface SCThingOrigin {
  /**
   * Maintainer of the origin
   *
   * e.g. restaurant of a dish
   */
  maintainer?: SCPersonWithoutReferences | SCOrganizationWithoutReferences;

  /**
   * When the thing was modified last in the origin
   */
  modified?: SCISO8601Date;

  /**
   * Type of the origin
   */
  type: SCThingOriginType;
}

/**
 * Remote origin of a thing
 */
export interface SCThingRemoteOrigin extends SCThingOrigin {
  /**
   * When the thing was indexed last from the origin
   */
  indexed: SCISO8601Date;

  /**
   * Name of the origin
   *
   * @text
   */
  name: string;

  /**
   * Original ID of the thing in the origin
   */
  originalId?: string;

  /**
   * Entity that is responsible for the entity
   *
   * e.g. an organizer for an event
   */
  responsibleEntity?: SCPersonWithoutReferences | SCOrganizationWithoutReferences;

  /**
   * Type of the origin
   */
  type: SCThingOriginType.Remote;

  /**
   * Main URL of the origin
   */
  url?: string;
}

/**
 * User origin of a thing (data created through user interaction)
 */
export interface SCThingUserOrigin extends SCThingOrigin {
  /**
   * When the thing was created
   */
  created: SCISO8601Date;

  /**
   * If it is deleted or not, defaults to false
   */
  deleted?: boolean;

  /**
   * Type of the origin
   */
  type: SCThingOriginType.User;

  /**
   * When the saved thing was last updated with the latest state (e.g. from the backend)
   */
  updated?: SCISO8601Date;
}

/**
 * Translatable properties of things
 */
export interface SCThingTranslatableProperties {
  /**
   * Translation of the description of the thing
   *
   * @text
   */
  description?: string;
  /**
   * Translation of the name of the thing
   *
   * @sortable ducet
   * @text
   */
  name?: string;
  /**
   * Origin of the thing
   */
  origin?: SCThingTranslatablePropertyOrigin;
}

/**
 * Translatable property of an origin
 */
export interface SCThingTranslatablePropertyOrigin {
  /**
   * Translation of the name of the origin
   *
   * @text
   */
  name: string;
}

/**
 * Meta information about things
 */
export class SCThingMeta implements SCMetaTranslations<SCThing> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      alternateNames: 'alternative Namen',
      description: 'Beschreibung',
      identifiers: 'Alternative Identifikation',
      image: 'Bild',
      name: 'Name',
      origin: 'Ursprung',
      sameAs: 'ursprünglicher Link',
      translations: 'Übersetzungen',
      type: 'Typ',
      uid: 'Identifikation',
    },
    en: {
      alternateNames: 'alternate names',
      description: 'description',
      identifiers: 'alternative identification',
      image: 'image',
      name: 'name',
      origin: 'origin',
      sameAs: 'original link',
      translations: 'translations',
      type: 'type',
      uid: 'identification',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      type: 'Ding',
    },
    en: {
      type: 'Thing',
    },
  };
}
