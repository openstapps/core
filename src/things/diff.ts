/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import * as jsonpatch from 'json-patch';
import {SCMetaTranslations} from '../general/i18n';
import {SCISO8601Date} from '../general/time';
import {SCIndexableThings} from '../meta';
import {SCThing, SCThingMeta, SCThingType, SCThingWithoutReferences} from './abstract/thing';

/**
 * A diff without references
 */
export interface SCDiffWithoutReferences extends SCThingWithoutReferences {
  /**
   * Action of the diff
   */
  action: 'changed' | 'removed';

  /**
   * Diff patch. Only when action === 'changed'
   */
  changes?: jsonpatch.OpPatch[];

  /**
   * Creation date of the diff.
   */
  dateCreated: SCISO8601Date;

  /**
   * Type of a diff
   */
  type: SCThingType.Diff;
}

/**
 * A diff
 *
 * @validatable
 */
export interface SCDiff extends SCDiffWithoutReferences, SCThing {
  /**
   * Original object the diff was generated on
   */
  object: SCIndexableThings;

  /**
   * Type of a diff
   */
  type: SCThingType.Diff;
}

/**
 * Meta information about a diff
 */
export class SCDiffMeta extends SCThingMeta implements SCMetaTranslations<SCDiff> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCThingMeta().fieldTranslations.de,
      action: 'Aktion',
      changes: 'Änderungen',
      dateCreated: 'Erstellungsdatum',
      object: 'Objekt',
    },
    en: {
      ...new SCThingMeta().fieldTranslations.en,
      action: 'action',
      changes: 'changes',
      dateCreated: 'date created',
      object: 'object',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCThingMeta().fieldValueTranslations.de,
      action: {
        changed: 'geändert',
        removed: 'gelöscht',
      },
      type: 'Unterschied',
    },
    en: {
      ...new SCThingMeta().fieldValueTranslations.en,
      type: SCThingType.Diff,
    },
  };
}
