/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations} from '../general/i18n';
import {
  SCAcademicTerm,
  SCAcademicTermWithoutReferences,
  SCAcademicTermWithoutReferencesMeta,
} from './abstract/academic-term';
import {SCThingMeta, SCThingType} from './abstract/thing';

/**
 * A semester without references
 */
export interface SCSemesterWithoutReferences extends SCAcademicTermWithoutReferences {
  /**
   * The short name of the semester, using the given pattern.
   *
   * @filterable
   * @pattern ^(WS|SS|WiSe|SoSe) [0-9]{4}(/[0-9]{2})?$
   * @keyword
   */
  acronym: string;

  /**
   * Type of the semester
   */
  type: SCThingType.Semester;
}

/**
 * A semester
 *
 * @validatable
 * @indexable
 */
export interface SCSemester extends SCSemesterWithoutReferences, SCAcademicTerm {
  /**
   * Type of the semester
   */
  type: SCThingType.Semester;
}

/**
 * Meta information about a semester
 */
export class SCSemesterMeta extends SCThingMeta implements SCMetaTranslations<SCSemester> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCAcademicTermWithoutReferencesMeta().fieldTranslations.de,
      acronym: 'Abkürzung',
      endDate: 'Ende',
      eventsEndDate: 'Vorlesungsschluss',
      eventsStartDate: 'Vorlesungsbeginn',
      startDate: 'Beginn',
    },
    en: {
      ...new SCAcademicTermWithoutReferencesMeta().fieldTranslations.en,
      acronym: 'acronym',
      endDate: 'end date',
      eventsEndDate: 'semester ending',
      eventsStartDate: 'semester beginning',
      startDate: 'start date',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCAcademicTermWithoutReferencesMeta().fieldValueTranslations.de,
      type: 'Semester',
    },
    en: {
      ...new SCAcademicTermWithoutReferencesMeta().fieldValueTranslations.en,
      type: SCThingType.Semester,
    },
  };
}
