/*
 * Copyright (C) 2021-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCLanguage, SCMetaTranslations, SCTranslations} from '../general/i18n';
import {
  SCAcademicDegree,
  SCAcademicDegreeMeta,
  SCAcademicDegreeWithoutReferences,
} from './abstract/academic-degree';
import {SCThingMeta, SCThingType} from './abstract/thing';
import {
  SCAcademicPriceGroup,
  SCThingThatCanBeOffered,
  SCThingThatCanBeOfferedMeta,
  SCThingThatCanBeOfferedTranslatableProperties,
  SCThingThatCanBeOfferedWithoutReferences,
} from './abstract/thing-that-can-be-offered';
import {SCDateSeriesWithoutReferences} from './date-series';
import {SCOrganizationWithoutReferences} from './organization';

/**
 * A course of study without references
 */
export interface SCCourseOfStudyWithoutReferences
  extends SCAcademicDegreeWithoutReferences,
    SCThingThatCanBeOfferedWithoutReferences {
  /**
   * The main language in which the course of study
   * is beeing offered
   */
  mainLanguage?: SCLanguage;

  /**
   * The modes the course of study is offered in
   *
   * @filterable
   */
  mode?: SCCourseOfStudyMode;

  /**
   * The time modes the course of study is offered in
   *
   * @filterable
   */
  timeMode?: SCCourseOfStudyTimeMode;

  /**
   * Translated fields of a dish
   */
  translations?: SCTranslations<SCCourseOfStudyTranslatableProperties>;

  /**
   * Type of the course of study
   */
  type: SCThingType.CourseOfStudy;
}

/**
 * A course of study
 *
 * @validatable
 * @indexable
 */
export interface SCCourseOfStudy
  extends SCCourseOfStudyWithoutReferences,
    SCThingThatCanBeOffered<SCAcademicPriceGroup>,
    SCAcademicDegree {
  /**
   * The department that manages the course of study
   */
  department?: SCOrganizationWithoutReferences;

  /**
   * The secretary that administers requests and
   * questions concerning the course of study
   */
  secretary?: SCOrganizationWithoutReferences;

  /**
   * Dates at which the course of study is planned to start
   */
  startDates?: SCDateSeriesWithoutReferences[];

  /**
   * Translated fields of a course of study
   */
  translations?: SCTranslations<SCCourseOfStudyTranslatableProperties>;

  /**
   * Type of the course of study
   */
  type: SCThingType.CourseOfStudy;
}

/**
 * Translatable properties of a course of study
 */
export type SCCourseOfStudyTranslatableProperties = SCThingThatCanBeOfferedTranslatableProperties;

/**
 * Meta information about a course of study
 */
export class SCCourseOfStudyMeta extends SCThingMeta implements SCMetaTranslations<SCCourseOfStudy> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCAcademicDegreeMeta().fieldTranslations.de,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldTranslations.de,
      department: 'Fachbereich',
      mainLanguage: 'Unterrichtssprache',
      mode: 'Studiengangsart',
      secretary: 'Sekretariat',
      startDates: 'Startdatum',
      timeMode: 'Zeitmodell',
    },
    en: {
      ...new SCAcademicDegreeMeta().fieldTranslations.en,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldTranslations.de,
      department: 'department',
      mainLanguage: 'main language',
      mode: 'mode',
      secretary: 'secretary',
      startDates: 'start dates',
      timeMode: 'time mode',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCAcademicDegreeMeta().fieldValueTranslations.de,
      modes: {
        'combination': 'Kombinationsstudiengang',
        'double-degree': 'Doppelstudium',
        'dual': 'duales Studium',
        'standard': 'Studium',
      },
      timeMode: {
        fulltime: 'Vollzeitstudiengang',
        parttime: 'Teilzeitstudiengang',
      },
      type: 'Studiengang',
    },
    en: {
      ...new SCAcademicDegreeMeta().fieldValueTranslations.en,
      modes: {
        'combination': 'combination course of study',
        'double-degree': 'double degree course of study',
        'dual': 'dual course of study',
        'standard': 'course of study',
      },
      timeMode: {
        fulltime: 'full-time',
        parttime: 'part-time',
      },
      type: SCThingType.CourseOfStudy,
    },
  };
}

/**
 * Types of (german) course of study modes
 */
export type SCCourseOfStudyMode = 'combination' | 'dual' | 'double-degree' | 'standard';

/**
 * Types of (german) course of study time modes
 */
export type SCCourseOfStudyTimeMode = 'fulltime' | 'parttime';
