/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCMetaTranslations, SCTranslations} from '../general/i18n';
import {
  SCCreativeWork,
  SCCreativeWorkMeta,
  SCCreativeWorkTranslatableProperties,
  SCCreativeWorkWithoutReferences,
} from './abstract/creative-work';
import {SCThingMeta, SCThingType} from './abstract/thing';
import {
  SCAcademicPriceGroup,
  SCThingThatCanBeOffered,
  SCThingThatCanBeOfferedMeta,
  SCThingThatCanBeOfferedTranslatableProperties,
  SCThingThatCanBeOfferedWithoutReferences,
} from './abstract/thing-that-can-be-offered';
import {
  SCThingWithCategoriesSpecificValues,
  SCThingWithCategoriesTranslatableProperties,
  SCThingWithCategoriesWithoutReferences,
  SCThingWithCategoriesWithoutReferencesMeta,
} from './abstract/thing-with-categories';

/**
 * Categories of a book
 */
export type SCBookCategories =
  | 'audio'
  | 'audiobook'
  | 'book'
  | 'cd'
  | 'dvd'
  | 'eAudiobook'
  | 'ebook'
  | 'ePhoto'
  | 'hierarchy'
  | 'kit'
  | 'manuscript'
  | 'map'
  | 'microfilm'
  | 'musicalscore'
  | 'photo'
  | 'physicalobject'
  | 'retro'
  | 'sensorimage'
  | 'unknown'
  | 'video';

/**
 * A book without references
 */
export interface SCBookWithoutReferences
  extends SCCreativeWorkWithoutReferences,
    SCThingThatCanBeOfferedWithoutReferences,
    SCThingWithCategoriesWithoutReferences<SCBookCategories, SCThingWithCategoriesSpecificValues> {
  /**
   * Categories of a book
   */
  categories: SCBookCategories[];

  /**
   * ISBNs of a book
   *
   * @filterable
   * @keyword
   */
  ISBNs?: string[];

  /**
   * Number of pages of a book
   *
   * @integer
   */
  numberOfPages?: number;

  /**
   * Translated properties of a book
   */
  translations?: SCTranslations<SCBookTranslatableFields>;

  /**
   * Type of a book
   */
  type: SCThingType.Book;
}

/**
 * A book
 *
 * @validatable
 * @indexable
 */
export interface SCBook
  extends SCCreativeWork,
    SCThingThatCanBeOffered<SCAcademicPriceGroup>,
    SCBookWithoutReferences {
  /**
   * Translated properties of a book
   */
  translations?: SCTranslations<SCBookTranslatableFields>;

  /**
   * Type of a book
   */
  type: SCThingType.Book;
}

/**
 * Translatable properties of a book
 */
export interface SCBookTranslatableFields
  extends SCThingWithCategoriesTranslatableProperties,
    SCThingThatCanBeOfferedTranslatableProperties,
    SCCreativeWorkTranslatableProperties {}

/**
 * Meta information about a book
 */
export class SCBookMeta extends SCThingMeta implements SCMetaTranslations<SCBook> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ...new SCCreativeWorkMeta().fieldTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCBookCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.de,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldTranslations.de,
      categories: 'Format',
      ISBNs: 'ISBN',
      numberOfPages: 'Seitenanzahl',
    },
    en: {
      ...new SCCreativeWorkMeta().fieldTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCBookCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldTranslations.en,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldTranslations.en,
      categories: 'format',
      ISBNs: 'ISBN',
      numberOfPages: 'number of pages',
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations = {
    de: {
      ...new SCCreativeWorkMeta().fieldValueTranslations.de,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCBookCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.de,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldValueTranslations.de,
      categories: {
        audio: 'Tonträger',
        audiobook: 'Hörbuch',
        book: 'Buch',
        cd: 'CD',
        dvd: 'DVD',
        eAudiobook: 'E-Hörbuch',
        ebook: 'E-Book',
        ePhoto: 'E-Photo',
        hierarchy: 'mehrteiliges Werk',
        kit: 'Medienkombination',
        manuscript: 'Handschrift',
        map: 'Karte',
        microfilm: 'Mikrofilm, Mikrofiche',
        musicalscore: 'Noten',
        photo: 'Abbildung',
        physicalobject: 'Objekt',
        retro: 'Retro (Buch)',
        sensorimage: 'Blindenschrift',
        unknown: 'Unbekannt',
        video: 'Film',
      },
      type: 'Buch',
    },
    en: {
      ...new SCCreativeWorkMeta().fieldValueTranslations.en,
      ...new SCThingWithCategoriesWithoutReferencesMeta<
        SCBookCategories,
        SCThingWithCategoriesSpecificValues
      >().fieldValueTranslations.en,
      ...new SCThingThatCanBeOfferedMeta<SCAcademicPriceGroup>().fieldValueTranslations.en,
      type: SCThingType.Book,
      categories: {
        audio: 'audio material',
        audiobook: 'audiobook',
        book: 'book',
        cd: 'CD',
        dvd: 'DVD',
        eAudiobook: 'E-Audiobook',
        ebook: 'E-Book',
        ePhoto: 'E-Photo',
        hierarchy: 'multipart item',
        kit: 'media combination',
        manuscript: 'manuscript',
        map: 'map',
        microfilm: 'microfilm, microfiche',
        musicalscore: 'sheet music',
        photo: 'illustration',
        physicalobject: 'object',
        retro: 'retro (book)',
        sensorimage: 'braille',
        unknown: 'unknown',
        video: 'film',
      },
    },
  };
}
