/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCSaveableThing, SCSaveableThingWithoutReferences} from './abstract/saveable-thing';
import {SCThingMeta, SCThingType} from './abstract/thing';

/**
 * A favorite without references
 */
export interface SCFavoriteWithoutReferences extends SCSaveableThingWithoutReferences {
  /**
   * Type of a favorite
   */
  type: SCThingType.Favorite;
}

/**
 * A favorite
 *
 * @validatable
 */
export interface SCFavorite extends SCSaveableThing {
  /**
   * Type of a favorite
   */
  type: SCThingType.Favorite;
}

/**
 * Meta information about a favorite
 */
export class SCFavoriteMeta extends SCThingMeta {}
