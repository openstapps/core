# @openstapps/core

[![pipeline status](https://img.shields.io/gitlab/pipeline/openstapps/core.svg?style=flat-square)](https://gitlab.com/openstapps/core/commits/master)
[![npm](https://img.shields.io/npm/v/@openstapps/core.svg?style=flat-square)](https://npmjs.com/package/@openstapps/core)
[![license)](https://img.shields.io/npm/l/@openstapps/core.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![documentation](https://img.shields.io/badge/documentation-online-blue.svg?style=flat-square)](https://openstapps.gitlab.io/core)

StAppsCore - Generalized model of data

## What is the StAppsCore?

The StAppsCore (SC) is a generalized model of data.

SC is based on the idea of [schema.org](https://www.schema.org). The SC structures data by describing objects as so called SC-types like: Event, Place, Date, Person, ...

Data sources of the same type of data are assigned to the same SC-type.

### Requirements for the SC

- Study processes which can be generalized are abstracted and modeled in that manner that the structure can be adapted to any german university.
- It must be platform- and software-independent.
- Search interface must be clearly specified that different working groups with different search tools can retrieve the same search results.
- Must be expandable.

### Generate documentation for routes

To generate a documentation for the routes use the following command.

```shell
node --require ts-node/register src/cli.ts routes PATH/TO/ROUTES.md
```

### Annotations

Annotations are used to add additional informations to fields, which are used to autogenerate mappings from the core objects.
External dependencies can not be covered by the annotations. Documentation about some of the annotations can be found in: [typedoc](https://typedoc.org/guides/doccomments/)

| annotation      | description                                                                                                                                                         | parameters                                                                       |
| --------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------- |
| `@aggregatable` | used for generating of aggregations of the field if the core schema is used to put data into a database/key-value store                                             | whether the property is being used on the top type or across all types: `global` |
| `@float`        | number field is interpreted as float                                                                                                                                |                                                                                  |
| `@indexable`    | marks the type as indexable if the core schema is used to put data into a database/key-value store                                                                  |                                                                                  |
| `@integer`      | number field is interpreted as integer                                                                                                                              |                                                                                  |
| `@keyword`      | string field is interpreted as keyword                                                                                                                              |                                                                                  |
| `@sortable`     | field is sortable if the core schema is used to put data into a database/key-value store. Fields are always sortable through generic sort, even without annotation. | sort method to be used: `ducet`, `price`, `distance`                             |
| `@text`         | string field is interpreted as text                                                                                                                                 |                                                                                  |
| `@date`         | string field is interpreted as a date field                                                                                                                         |                                                                                  |
| `@validatable`  | Marks the type for json schema generation, which can be checked at runtime. Any top-level type that is put into a database should always be marked as validatable.  |                                                                                  |
| `@filterable`   | non-object/nested field is filterable if the core schema is used to put data into a database/key-value store                                                        |                                                                                  |
| `@inheritTags`  | inherit all tags from another field                                                                                                                                 | `[SCThingType]::[field]`                                                         |

_Note: tags ignore casing, but for consistency they should use the variants proposed here._
